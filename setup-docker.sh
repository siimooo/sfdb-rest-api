#!/bin/bash
# Edit the following three values to your liking:
MYSQL_USER="root"
MYSQL_DATABASE="sfdb"
MYSQL_CONTAINER_NAME="mysql-server"

MYSQL_ROOT_PASSWORD="root"
MYSQL_PASSWORD="root"

# Create an overlay network named mysql to allow multiple containers to communicate with one another.
docker network create mysql

#Create a data volume container named mysql-data. The MySQL image stores its table data in /var/lib/mysql, so include --volume /var/lib/mysql to present that path as a volume that other containers can mount. Include --volume /backups to create a persistent folder for your database backups.
docker create --name mysql-data --volume /var/lib/mysql --volume /backups mysql:latest

# start the database MySQL container
# Start a MySQL container with a database named test. Include --volumes-from mysql-data to mount the volumes provided by that container.
docker run -d --name ${MYSQL_CONTAINER_NAME} --net mysql --volumes-from mysql-data -e MYSQL_USER=${MYSQL_USER} -e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD} -e MYSQL_DATABASE=${MYSQL_DATABASE} mysql:latest

# copy conf
docker cp my.cnf mysql-server:/etc/mysql/

echo "Sleeping for 15 seconds -> Allow time for the DB to be initialized:"
for i in `seq 1 15`;
do
  echo "sleep..."
  sleep 1
done

# restart database server
docker restart mysql-server

echo "Sleeping for 15 seconds -> Create SFDB database"
for i in `seq 1 15`;
do
  echo "sleep..."
  sleep 1
done

# Dump database with sfdb-rest-api
docker exec -i mysql-server mysql -u root -proot < sfdb_db.sql

echo "Database '${MYSQL_DATABASE}' running."
echo "  Username: ${MYSQL_USER}"
echo "  Password: ${MYSQL_PASSWORD}"

echo "Build the REST application"
# build the application
gradle build -x test

# build the sfdb-rest-api container
docker build -t siimoo/sfdb-rest-api .

# start the sfdb-rest-api container
# docker run -d --net foodtrucks -p 5000:5000 --name foodtrucks-web prakhar1989/foodtrucks-web
#         deamon -m 300M memory limit port <CONTAINER NAME> < IMAGE>
#                <WORLD:CONTAINER>
docker run -d -p 8080:8888 --net mysql --name sfdb-rest-api siimoo/sfdb-rest-api