package sfdb.service.omdb;

import io.rxjavaomdb.network.moshi.OMDbTitleOrId;
import io.sfdb.SFDbApplication;
import io.sfdb.resource.omdb.OMDbMovieResource;
import io.sfdb.resource.omdb.model.OMDbMovie;
import io.sfdb.resource.themoviedb.TheMovieDBMovieOverviewTranslationResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import io.sfdb.service.omdb.OMDbService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import sfdb.service.themoviedb.TheMovieDBServiceTest;

import java.util.Date;

/**
 * User: Simo Ala-Kotila
 * Date: 09/03/17
 * Time: 09:50
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class OMDbServiceTest {

    @Autowired
    private TheMovieDBMovieResource theMovieDBMovieResource;

    @Autowired
    private OMDbService omDbService;

    @Autowired
    private OMDbMovieResource omDbMovieResource;

    @Autowired
    private TheMovieDBMovieOverviewTranslationResource theMovieDBMovieOverviewTranslationResource;

    @Test
    public void fromSuccessTest(){
        String imdbId = "tt0944947";
        OMDbTitleOrId omDbTitleOrId = omDbService.fromOMDb(imdbId);
        Assert.assertNotNull(omDbTitleOrId);
    }

    @Test
    public void saveSuccessTest(){
        String imdbId = "tt0944947";
        OMDbTitleOrId omDbTitleOrId = omDbService.fromOMDb(imdbId);
        Assert.assertNotNull(omDbTitleOrId);
        omDbService.saveMovie(omDbTitleOrId);
    }

    @Test
    public void saveSuccessTheMovieDbTest(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        omDbMovieResource.deleteAll();
        int theMovieDbId = 111;
        String imdbId = "tt0944947";
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId, "fi", imdbId);
        theMovieDBMovieResource.save(theMovieDBMovie2);
        omDbService.saveMovie(theMovieDbId, true);
        OMDbMovie byImdbId = omDbMovieResource.findByImdbId(imdbId);
        Assert.assertNotNull(byImdbId);
    }

    @Test
    public void saveFailureTheMovieDbTest(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        omDbMovieResource.deleteAll();
        int theMovieDbId = 111;
        String imdbId = "tt0947";
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId);
        theMovieDBMovieResource.save(theMovieDBMovie2);
        omDbService.saveMovie(theMovieDbId, true);
        OMDbMovie byImdbId = omDbMovieResource.findByImdbId(imdbId);
        Assert.assertNull(byImdbId);
    }


    @Test
    public void fromSuccessFailure(){
        String imdbId = "tt094";
        OMDbTitleOrId omDbTitleOrId = omDbService.fromOMDb(imdbId);
        Assert.assertNull(omDbTitleOrId);
    }
}