package sfdb.service.themoviedb;

import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBDetails;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.sfdb.SFDbApplication;
import io.sfdb.resource.themoviedb.*;
import io.sfdb.resource.themoviedb.model.movie.*;
import io.sfdb.service.themoviedb.TheMovieDBCollectionServiceV2;
import io.sfdb.service.themoviedb.TheMovieDBGenreService;
import io.sfdb.service.themoviedb.TheMovieDBMovieService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

/**
 * This with real data
 *
 * User: Simo Ala-Kotila
 * Date: 11/01/17
 * Time: 16:47
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class TheMovieDBServiceTest {

    @Autowired
    private TheMovieDBMovieService theMovieDBMovieService;

    @Autowired
    private TheMovieDBMovieResource theMovieDBMovieResource;

    @Autowired
    private TheMovieDBCollectionResource theMovieDBCollection;

    @Autowired
    private TheMovieDBCollectionServiceV2 theMovieDBCollectionService;

    @Autowired
    private TheMovieDBGenreService theMovieDBGenreService;

    @Autowired
    private TheMovieDBMovieGenreResource theMovieDBMovieGenreResource;

    @Autowired
    private TheMovieDBMovieGenreListResource theMovieDBMovieGenreListResource;

    @Autowired
    private TheMovieDBMovieOverviewTranslationResource theMovieDBMovieOverviewTranslationResource;

    @Autowired
    private TheMovieDBMovieReleaseDateResource theMovieDBMovieReleaseDateResource;

    @Autowired
    private TheMovieDBCollectionTranslationResource theMovieDBCollectionTranslationResource;

    @Test
    public void testUpcomings(){
        List<TheMovieDBMovieUpcomingResults> theMovieDBMovieUpcomingResultsList = theMovieDBMovieService.upcomings();
        Assert.assertNotNull(theMovieDBMovieUpcomingResultsList);
        Assert.assertFalse(theMovieDBMovieUpcomingResultsList.isEmpty());
    }

    @Test
    public void testDetailsNotEmpty(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        TheMovieDBMovie2 theMovieDBMovie2 = dummy();
        theMovieDBMovieResource.save(theMovieDBMovie2);
        Assert.assertFalse(theMovieDBMovieService.isMovieDetailEmpty());
    }

    public static TheMovieDBMovie2 dummy(){
        return dummy(1, "fi", "123");
    }

    public static TheMovieDBMovie2 dummy(int theMovieDbID){
        return dummy(theMovieDbID, "fi", "11");
    }

    public static TheMovieDBMovie2 dummy(int theMovieDbID, String originalLanguage, String imdbID){
        TheMovieDBMovie2 theMovieDBMovie2 = new TheMovieDBMovie2();
        theMovieDBMovie2.theMovieDbId = theMovieDbID;
        theMovieDBMovie2.imdbId = imdbID;
        theMovieDBMovie2.backdropPath = "1";
        theMovieDBMovie2.budget = 11;
        theMovieDBMovie2.original_language = originalLanguage;
        theMovieDBMovie2.original_title = "sad";
        theMovieDBMovie2.popularity = 1.1;
        theMovieDBMovie2.poster_path = "a";
        theMovieDBMovie2.revenue = 1;
        theMovieDBMovie2.runtime = 1;
        theMovieDBMovie2.status = "1";
        theMovieDBMovie2.tagline = "12";
        theMovieDBMovie2.title = "12";
        theMovieDBMovie2.video = true;
        theMovieDBMovie2.vote_average = 1.1;
        theMovieDBMovie2.vote_count = 1;
        return theMovieDBMovie2;
    }

    @Test
    public void testDetailsIsEmpty(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        Assert.assertTrue(theMovieDBMovieService.isMovieDetailEmpty());
    }

    public TheMovieDBMovieUpcomingResults theMovieDBMovieUpcomingResults(){

        String posterPath = "asd";
        boolean adult = false;
        String overview = "3";
        double poula = 1.12;
        int votecount = 1;
        int id = 1891;
        String language = "11";
        String originalTitle = "1";
        String title = "1";
        List<Integer> genres = new ArrayList<>();
        TheMovieDBMovieUpcomingResults theMovieDBMovieUpcomingResults = new TheMovieDBMovieUpcomingResults(posterPath,
                                                                        adult,
                                                                        overview,
                                                                        poula, votecount, id, language, genres, originalTitle, false, new Date(System.currentTimeMillis()).toString(), title);



        return theMovieDBMovieUpcomingResults;
    }

    @Test
    public void testSaveDetailsWithBelongsToCollectionId(){
        theMovieDBCollection.deleteAll();
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        theMovieDBMovieGenreResource.deleteAll();
        int id = 283995;
        int collectionId = 284433;
        theMovieDBMovieService.saveMovieDetails(id);
        TheMovieDBMovie2 byTheMovieDbId = theMovieDBMovieResource.findByTheMovieDbId(id);
        Assert.assertNotNull(byTheMovieDbId.belongs_to_collection);
        Assert.assertTrue(byTheMovieDbId.belongs_to_collection == collectionId);
    }

    @Test
    public void testSaveDetailsWithNullBelongsToCollectionId(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        theMovieDBCollectionTranslationResource.deleteAll();
        theMovieDBCollection.deleteAll();
        theMovieDBMovieGenreResource.deleteAll();

        TheMovieDBMovieUpcomingResults theMovieDBMovieUpcomingResults = theMovieDBMovieService.upcomings().get(2);
        List<Integer> genreIds = theMovieDBMovieUpcomingResults.getGenreIds();
        for (Integer genreId : genreIds) {
            TheMovieDBGenreList2 theMovieDBGenreList2 = new TheMovieDBGenreList2();
            theMovieDBGenreList2.genreId = genreId;
            theMovieDBMovieGenreListResource.save(theMovieDBGenreList2);
        }

        theMovieDBMovieService.saveUpcoming(theMovieDBMovieUpcomingResults);
        TheMovieDBMovie2 byTheMovieDbId = theMovieDBMovieResource.findByTheMovieDbId(theMovieDBMovieUpcomingResults.getId());
        Assert.assertNull(byTheMovieDbId.belongs_to_collection);
    }

    @Test
    public void testSaveOverview(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieService.saveOverview(1, "asdssad", new Locale("fi", "FI"));
        TheMovieDBMovieOverviewTranslation2 fi = theMovieDBMovieOverviewTranslationResource.findByMovieDBIdAndLanguage(1, "fi");
        Assert.assertNotNull(fi);
    }

    @Test
    public void testSaveMovieDetailsLocale(){
        theMovieDBMovieResource.deleteAll();
        theMovieDBCollection.deleteAll();
        theMovieDBMovieGenreResource.deleteAll();
        boolean b2 = theMovieDBMovieService.saveMovieDetails(121856);
        theMovieDBMovieService.saveOverview(121856, new Locale("fi", "FI"));
        TheMovieDBMovieOverviewTranslation2 fi = theMovieDBMovieOverviewTranslationResource.findByMovieDBIdAndLanguage(121856, "fi");
        TheMovieDBMovieOverviewTranslation2 en = theMovieDBMovieOverviewTranslationResource.findByMovieDBIdAndLanguage(121856, "en");
        Assert.assertNotNull(fi);
        Assert.assertNotNull(en);
        Assert.assertFalse(fi.overview.equals(en.overview));
    }

    @Test
    public void testSaveMovieReleaseDates(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        theMovieDBMovieResource.save(dummy(121856));
        theMovieDBMovieService.saveReleaseDates(121856);
        Iterable<TheMovieDBMovieReleaseDate2> all = theMovieDBMovieReleaseDateResource.findAll();
        Assert.assertNotNull(all);
    }
}