package sfdb.service.themoviedb;

import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBDBelongsToCollection;
import io.sfdb.SFDbApplication;
import io.sfdb.resource.themoviedb.TheMovieDBCollectionResource;
import io.sfdb.resource.themoviedb.TheMovieDBCollectionTranslationResource;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBCollectionTranslation2;
import io.sfdb.service.LocaleHelper;
import io.sfdb.service.themoviedb.TheMovieDBCollectionServiceV2;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Locale;

/**
 * User: Simo Ala-Kotila
 * Date: 17/02/17
 * Time: 13:04
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class TheMovieDBCollectionServiceTest {

    @Autowired
    private TheMovieDBCollectionServiceV2 theMovieDBCollectionService;

    @Autowired
    private TheMovieDBCollectionResource theMovieDBCollectionResource;

    @Autowired
    private TheMovieDBCollectionTranslationResource theMovieDBCollectionTranslationResource;

    @Test
    public void nullTest(){
        Assert.assertFalse(theMovieDBCollectionService.saveCollection(null));
    }

    private TheMovieDBDBelongsToCollection get(int id) {
        return new TheMovieDBDBelongsToCollection(id, "asd");
    }

    @Test
    public void alreadyInDbTest(){
        theMovieDBCollectionTranslationResource.deleteAll();
        theMovieDBCollectionResource.deleteAll();

        TheMovieDBDBelongsToCollection theMovieDBDBelongsToCollection = get(10);
        theMovieDBCollectionService.saveCollection(theMovieDBDBelongsToCollection);
        boolean b = theMovieDBCollectionService.saveCollection(theMovieDBDBelongsToCollection);
        Assert.assertFalse(b);
    }

    @Test
    public void okTestDbTest(){
        theMovieDBCollectionTranslationResource.deleteAll();
        theMovieDBCollectionResource.deleteAll();
        TheMovieDBDBelongsToCollection theMovieDBDBelongsToCollection = get(10);
        boolean b = theMovieDBCollectionService.saveCollection(theMovieDBDBelongsToCollection);
        Assert.assertTrue(b);
    }

    @Test
    public void testSaveTranslation(){
        theMovieDBCollectionResource.deleteAll();
        theMovieDBCollectionTranslationResource.deleteAll();
        TheMovieDBDBelongsToCollection theMovieDBDBelongsToCollection = get(10);
        boolean b = theMovieDBCollectionService.saveCollection(theMovieDBDBelongsToCollection);
        theMovieDBCollectionService.saveCollectionTranslation(theMovieDBDBelongsToCollection.getId(), Locale.ENGLISH);
        theMovieDBCollectionService.saveCollectionTranslation(theMovieDBDBelongsToCollection.getId(), LocaleHelper.fi());

        TheMovieDBCollectionTranslation2 byCollectionIdIdAndLanguageEN = theMovieDBCollectionTranslationResource.findByCollectionIdIdAndLanguage(theMovieDBDBelongsToCollection.getId(), Locale.ENGLISH.getLanguage());
        TheMovieDBCollectionTranslation2 byCollectionIdIdAndLanguageFI = theMovieDBCollectionTranslationResource.findByCollectionIdIdAndLanguage(theMovieDBDBelongsToCollection.getId(), LocaleHelper.fi().getLanguage());
        Assert.assertNotNull(byCollectionIdIdAndLanguageEN);
        Assert.assertNotNull(byCollectionIdIdAndLanguageFI);
    }
}
