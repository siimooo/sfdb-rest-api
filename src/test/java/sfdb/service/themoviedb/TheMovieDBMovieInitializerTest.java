package sfdb.service.themoviedb;

import io.sfdb.SFDbApplication;
import io.sfdb.resource.themoviedb.MovieInitializer;
import io.sfdb.resource.themoviedb.TheMovieDBCollectionResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreListResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * User: Simo Ala-Kotila
 * Date: 17/02/17
 * Time: 11:47
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class TheMovieDBMovieInitializerTest {

    @Autowired
    private TheMovieDBMovieGenreListResource theMovieDBMovieGenreListResource;

    @Autowired
    private TheMovieDBMovieResource theMovieDBMovieResource;

    @Autowired
    private TheMovieDBCollectionResource theMovieDBCollectionResource;

    @Autowired
    private MovieInitializer movieInitializer;

    @Ignore
    @Test
    public void testInitializer(){
        theMovieDBCollectionResource.deleteAll();
        theMovieDBMovieGenreListResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        movieInitializer.init(null);
    }
}
