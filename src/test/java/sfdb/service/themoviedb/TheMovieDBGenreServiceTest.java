package sfdb.service.themoviedb;

import io.rxjavathemoviedb.network.moshi.TheMovieDBGenre;
import io.sfdb.SFDbApplication;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreListResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreList2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieGenre2;
import io.sfdb.service.themoviedb.TheMovieDBGenreService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * This with real data
 *
 * User: Simo Ala-Kotila
 * Date: 11/01/17
 * Time: 16:47
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class TheMovieDBGenreServiceTest {

    @Autowired
    private TheMovieDBGenreService theMovieDBGenreService;

    @Autowired
    private TheMovieDBMovieGenreListResource theMovieDBMovieGenreListResource;

    @Autowired
    private TheMovieDBMovieResource theMovieDBMovieResource;

    @Autowired
    private TheMovieDBMovieGenreResource theMovieDBMovieGenreResource;

    @Test
    public void testGenres(){
        List<TheMovieDBGenre> genres = theMovieDBGenreService.genres();
        Assert.assertNotNull(genres);
        Assert.assertFalse(genres.isEmpty());
    }

    @Test
    public void testIsGenresEmptyTrue(){
        Assert.assertTrue(theMovieDBGenreService.isGenresEmpty());
    }

    @Test
    public void testSaveMovieGenre(){
        theMovieDBGenreService.clearDB();
        Integer genreId = 1;
        Integer genreId2 = 2;

        TheMovieDBMovie2 dummy = TheMovieDBServiceTest.dummy();
        theMovieDBMovieResource.save(dummy);
        TheMovieDBGenreList2 action = new TheMovieDBGenreList2();
        action.genreId = genreId;
        TheMovieDBGenreList2 drama = new TheMovieDBGenreList2();
        drama.genreId = 2;
        theMovieDBMovieGenreListResource.save(action);
        theMovieDBMovieGenreListResource.save(drama);
        int movieId = 111;
        theMovieDBGenreService.saveMovieGenre(Arrays.asList(genreId, genreId2), movieId);
        List<TheMovieDBMovieGenre2> genresByTheMovieDbId = theMovieDBMovieGenreResource.findGenresByTheMovieDbId(movieId);
        Assert.assertNotNull(genresByTheMovieDbId.get(0).genre_id);
        Assert.assertTrue(genresByTheMovieDbId.get(0).theMovieDbId == 111);
    }

    @Test
    public void testIsGenresEmptyFalse(){
        List<TheMovieDBGenre> genres = theMovieDBGenreService.genres();
        TheMovieDBGenre theMovieDBGenre = genres.get(0);
        theMovieDBGenreService.saveGenreList(theMovieDBGenre);
        Assert.assertFalse(theMovieDBGenreService.isGenresEmpty());
    }

    @Test
    public void testSaveGenre(){
        List<TheMovieDBGenre> genres = theMovieDBGenreService.genres();
        TheMovieDBGenre theMovieDBGenre = genres.get(0);
        theMovieDBGenreService.saveGenreList(theMovieDBGenre);
        TheMovieDBGenreList2 one = theMovieDBMovieGenreListResource.findTheMovieGenreByGenreId(theMovieDBGenre.getId());
        Assert.assertNotNull(one);
    }

    @Test
    public void testSaveTranslation(){
        Integer genreId = 1;
        TheMovieDBGenre genreEN = new TheMovieDBGenre(genreId, "Action");
        TheMovieDBGenre genreFI = new TheMovieDBGenre(genreId, "Toiminta");
        Locale locale = new Locale("fi", "FI");
        theMovieDBGenreService.saveGenreListTranslation(genreFI, locale);
        theMovieDBGenreService.saveGenreListTranslation(genreEN, Locale.ENGLISH);
    }

   @After
   public void clear(){
       theMovieDBGenreService.clearDB();
   }
}