package sfdb.resource.themoviedb;

import io.sfdb.SFDbApplication;
import io.sfdb.resource.themoviedb.TheMovieDBCollectionResource;
import io.sfdb.resource.themoviedb.TheMovieDBCollectionTranslationResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBCollection2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBCollectionTranslation2;
import io.sfdb.service.LocaleHelper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Locale;

/**
 * User: Simo Ala-Kotila
 * Date: 17/02/17
 * Time: 11:45
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class TheMovieDBCollectionResourceTest {

    @Autowired
    private TheMovieDBCollectionResource theMovieDBCollectionResource;

    @Autowired
    private TheMovieDBCollectionTranslationResource theMovieDBCollectionTranslationResource;

    @Test
    public void testFindByCollectionId(){
        theMovieDBCollectionTranslationResource.deleteAll();
        theMovieDBCollectionResource.deleteAll();
        Integer id = 22;
        TheMovieDBCollection2 theMovieDBCollection2 = new TheMovieDBCollection2();
        theMovieDBCollection2.poster_path = null;
        theMovieDBCollection2.backdrop_path = null;
        theMovieDBCollection2.collectionId = id;

        theMovieDBCollectionResource.save(theMovieDBCollection2);
        TheMovieDBCollection2 byCollectionId = theMovieDBCollectionResource.findByCollectionId(id);
        Assert.assertNotNull(byCollectionId);
    }

    @Test
    public void testTranslation2(){
        theMovieDBCollectionTranslationResource.deleteAll();
        theMovieDBCollectionResource.deleteAll();

        Integer id = 22;
        TheMovieDBCollection2 theMovieDBCollection2 = new TheMovieDBCollection2();
        theMovieDBCollection2.poster_path = null;
        theMovieDBCollection2.backdrop_path = null;
        theMovieDBCollection2.collectionId = id;
        theMovieDBCollectionResource.save(theMovieDBCollection2);
        TheMovieDBCollectionTranslation2 byCollectionIdIdAndLanguage = theMovieDBCollectionTranslationResource.findByCollectionIdIdAndLanguage(id, LocaleHelper.fi().getLanguage());
        Assert.assertNull(byCollectionIdIdAndLanguage);

        TheMovieDBCollectionTranslation2 theMovieDBCollectionTranslation2 = new TheMovieDBCollectionTranslation2();
        theMovieDBCollectionTranslation2.name = "FI NAME";
        theMovieDBCollectionTranslation2.ISO_639_1 = LocaleHelper.fi().getLanguage();
        theMovieDBCollectionTranslation2.collectionId = id;

        TheMovieDBCollectionTranslation2 theMovieDBCollectionTranslation22 = new TheMovieDBCollectionTranslation2();
        theMovieDBCollectionTranslation22.name = "EN NAME";
        theMovieDBCollectionTranslation22.ISO_639_1 = Locale.ENGLISH.getLanguage();
        theMovieDBCollectionTranslation22.collectionId = id;

        theMovieDBCollectionTranslationResource.save(theMovieDBCollectionTranslation2);
        theMovieDBCollectionTranslationResource.save(theMovieDBCollectionTranslation22);
        TheMovieDBCollectionTranslation2 fi = theMovieDBCollectionTranslationResource.findByCollectionIdIdAndLanguage(id, LocaleHelper.fi().getLanguage());
        TheMovieDBCollectionTranslation2 en = theMovieDBCollectionTranslationResource.findByCollectionIdIdAndLanguage(id, Locale.ENGLISH.getLanguage());

        TheMovieDBCollection2 byCollectionIdWithTranslation = theMovieDBCollectionResource.findByCollectionIdWithTranslation(id);
        Assert.assertNotNull(byCollectionIdWithTranslation);
        Assert.assertNotNull(byCollectionIdWithTranslation.theMovieDBCollectionTranslation2s);
        List<TheMovieDBCollectionTranslation2> theMovieDBCollectionTranslation2s = byCollectionIdWithTranslation.theMovieDBCollectionTranslation2s;
        Assert.assertFalse(theMovieDBCollectionTranslation2s.isEmpty());
        Assert.assertTrue(theMovieDBCollectionTranslation2s.size() == 2);
    }
}