package sfdb.resource.themoviedb;

import io.rxjavathemoviedb.network.moshi.TheMovieDBGenre;
import io.sfdb.SFDbApplication;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreListResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreListTranslationResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreList2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreListTranslation2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieGenre2;
import io.sfdb.service.LocaleHelper;
import io.sfdb.service.themoviedb.TheMovieDBGenreService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import sfdb.service.themoviedb.TheMovieDBServiceTest;

import java.util.List;
import java.util.Locale;

/**
 * User: Simo Ala-Kotila
 * Date: 24/02/17
 * Time: 14:05
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class TheMovieDbGenreResourceTest {

    @Autowired
    private TheMovieDBMovieGenreListResource theMovieDBMovieGenreListResource;

    @Autowired
    private TheMovieDBMovieGenreResource theMovieDBMovieGenreResource;

    @Autowired
    private TheMovieDBMovieResource theMovieDBMovieResource;

    @Autowired
    private TheMovieDBMovieGenreListTranslationResource theMovieDBGenreListTranslation2;


    @Autowired
    private TheMovieDBGenreService genreService;

    @Test
    public void getByMovieDbId(){
        genreService.clearDB();

        theMovieDBMovieResource.save(TheMovieDBServiceTest.dummy(111));
        TheMovieDBGenreList2 action = new TheMovieDBGenreList2();
        action.genreId = 21;
        TheMovieDBGenreList2 drama = new TheMovieDBGenreList2();
        drama.genreId = 22;
        theMovieDBMovieGenreListResource.save(action);
        theMovieDBMovieGenreListResource.save(drama);

        TheMovieDBMovieGenre2 actiongenre = new TheMovieDBMovieGenre2();
        actiongenre.genre_id = 21;
        actiongenre.theMovieDbId = 111;
        TheMovieDBMovieGenre2 dramagenre = new TheMovieDBMovieGenre2();
        dramagenre.genre_id = 22;
        dramagenre.theMovieDbId = 111;

        theMovieDBMovieGenreResource.save(actiongenre);
        theMovieDBMovieGenreResource.save(dramagenre);
        List<TheMovieDBMovieGenre2> genresByTheMovieDbId = theMovieDBMovieGenreResource.findGenresByTheMovieDbId(111);
        Assert.assertNotNull(genresByTheMovieDbId.get(0).theMovieDBGenreList);
        Assert.assertNotNull(genresByTheMovieDbId.get(0).genre_id);
        Assert.assertTrue(genresByTheMovieDbId.get(0).theMovieDbId == 111);
    }

    @Test
    public void testTranslation(){
        genreService.clearDB();
        int genreId = 1;
        TheMovieDBGenreList2 theMovieDBGenreList2 = new TheMovieDBGenreList2();
        theMovieDBGenreList2.genreId = genreId;
        theMovieDBMovieGenreListResource.save(theMovieDBGenreList2);

        String fiLanguage = LocaleHelper.fi().getLanguage();
        String language = Locale.UK.getLanguage();

        TheMovieDBGenreListTranslation2 fiTranslation = new TheMovieDBGenreListTranslation2();
        fiTranslation.genreId = genreId;
        fiTranslation.ISO_639_1 = fiLanguage;
        fiTranslation.name = "Toiminta";
        theMovieDBGenreListTranslation2.save(fiTranslation);

        TheMovieDBGenreListTranslation2 enTranslation = new TheMovieDBGenreListTranslation2();
        enTranslation.genreId = genreId;
        enTranslation.ISO_639_1 = language;
        enTranslation.name = "Action";
        theMovieDBGenreListTranslation2.save(enTranslation);
    }


    @Test
    public void testTranslationJoin(){
        genreService.clearDB();
        int genreId = 1;
        TheMovieDBGenreList2 theMovieDBGenreList2 = new TheMovieDBGenreList2();
        theMovieDBGenreList2.genreId = genreId;
        theMovieDBMovieGenreListResource.save(theMovieDBGenreList2);

        TheMovieDBGenreList2 theMovieDBGenreList22 = new TheMovieDBGenreList2();
        theMovieDBGenreList22.genreId = 2;
        theMovieDBMovieGenreListResource.save(theMovieDBGenreList22);

        Locale fi = new Locale("fi", "FI");
        String fiLanguage = fi.getLanguage();

        String language = Locale.UK.getLanguage();

        TheMovieDBGenreListTranslation2 fiTranslation = new TheMovieDBGenreListTranslation2();
        fiTranslation.genreId = genreId;
        fiTranslation.ISO_639_1 = fiLanguage;
        fiTranslation.name = "Toiminta";
        theMovieDBGenreListTranslation2.save(fiTranslation);

        TheMovieDBGenreListTranslation2 enTranslation = new TheMovieDBGenreListTranslation2();
        enTranslation.genreId = genreId;
        enTranslation.ISO_639_1 = language;
        enTranslation.name = "Action";
        theMovieDBGenreListTranslation2.save(enTranslation);
        TheMovieDBGenreList2 theMovieGenreByGenreId = theMovieDBMovieGenreListResource.findGenreByGenreIdWithTranslastion(genreId);
        Assert.assertNotNull(theMovieGenreByGenreId);
        Assert.assertNotNull(theMovieGenreByGenreId.theMovieDBGenreListTranslation2s);
        Assert.assertFalse(theMovieGenreByGenreId.theMovieDBGenreListTranslation2s.isEmpty());
        Assert.assertTrue(theMovieGenreByGenreId.theMovieDBGenreListTranslation2s.size() == 2);
    }

    @Test
    public void testSaveTranslation(){
        genreService.clearDB();


        Integer genreId = 1;
        Locale fi = new Locale("fi", "FI");
        String fiLanguage = fi.getLanguage();
        
        TheMovieDBGenreList2 theMovieDBGenreList2 = new TheMovieDBGenreList2();
        theMovieDBGenreList2.genreId = genreId;
        theMovieDBMovieGenreListResource.save(theMovieDBGenreList2);

        String language = Locale.UK.getLanguage();
        TheMovieDBGenreListTranslation2 fiTranslation = new TheMovieDBGenreListTranslation2();
        fiTranslation.genreId = genreId;
        fiTranslation.ISO_639_1 = fiLanguage;
        fiTranslation.name = "Toiminta";
        theMovieDBGenreListTranslation2.save(fiTranslation);

        TheMovieDBGenreListTranslation2 enTranslation = new TheMovieDBGenreListTranslation2();
        enTranslation.genreId = genreId;
        enTranslation.ISO_639_1 = language;
        enTranslation.name = "Action";
        theMovieDBGenreListTranslation2.save(enTranslation);

        TheMovieDBGenreListTranslation2 fi1 = theMovieDBGenreListTranslation2.findByGenreIdAndLanguage(genreId, "fi");
        TheMovieDBGenreListTranslation2 en1 = theMovieDBGenreListTranslation2.findByGenreIdAndLanguage(genreId, "en");
        Assert.assertNotNull(fi1);
        Assert.assertNotNull(en1);
    }
}