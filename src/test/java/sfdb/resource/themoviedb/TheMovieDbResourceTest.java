package sfdb.resource.themoviedb;

import io.sfdb.SFDbApplication;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreListTranslationResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieOverviewTranslationResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieReleaseDateResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBCollectionTranslation2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieOverviewTranslation2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieReleaseDate2;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import sfdb.service.themoviedb.TheMovieDBServiceTest;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * This with real data
 *
 * User: Simo Ala-Kotila
 * Date: 11/01/17
 * Time: 16:47
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class TheMovieDbResourceTest {

    @Autowired
    private TheMovieDBMovieResource theMovieDBMovieResource;

    @Autowired
    private TheMovieDBMovieOverviewTranslationResource theMovieDBMovieOverviewTranslationResource;

    @Autowired
    private TheMovieDBMovieReleaseDateResource theMovieDBMovieReleaseDateResource;

    @Test
    public void findAll(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieReleaseDateResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy();
        theMovieDBMovieResource.save(theMovieDBMovie2);
        Pageable paginable = new PageRequest(0, 10);
        Page page = theMovieDBMovieResource.findAll(paginable);
        List<TheMovieDBMovie2> theMovieDBMovie2s = page.getContent();
        Assert.assertNotNull(theMovieDBMovie2s);
        Assert.assertTrue(theMovieDBMovie2s.size() == 1);
    }

    @Test
    public void findByTheMovieDbId(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        int theMovieDbId = 1111;
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId);
        theMovieDBMovieResource.save(theMovieDBMovie2);
        TheMovieDBMovie2 theMovieDBMovie22 = theMovieDBMovieResource.findByTheMovieDbId(theMovieDbId);
        Assert.assertNotNull(theMovieDBMovie22);
    }

    @Test
    public void findByOriginalLanguage(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        int theMovieDbId = 111;
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId, "fi", "123132");
        theMovieDBMovieResource.save(theMovieDBMovie2);

        List<TheMovieDBMovie2> fi = theMovieDBMovieResource.findByOriginalLanguage("fi");
        Assert.assertNotNull(fi);
        Assert.assertFalse(fi.isEmpty());
        Assert.assertNotNull(fi.get(0));
    }

    @Test
    public void findByTheMovieDbIdAndOriginalLanguage(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieReleaseDateResource.deleteAll();
        theMovieDBMovieResource.deleteAll();
        int theMovieDbId = 111;
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId);
        theMovieDBMovieResource.save(theMovieDBMovie2);

        List<TheMovieDBMovie2> fi = theMovieDBMovieResource.findByTheMovieDbIdAndOriginalLanguage(theMovieDbId, "fi");
        Assert.assertNotNull(fi);
        Assert.assertFalse(fi.isEmpty());
        Assert.assertNotNull(fi.get(0));
    }

    @Test
    public void testOverviewTranslation(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieReleaseDateResource.deleteAll();
        theMovieDBMovieResource.deleteAll();

        int theMovieDbId = 1;
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId);
        theMovieDBMovieResource.save(theMovieDBMovie2);

        TheMovieDBMovieOverviewTranslation2 fi = new TheMovieDBMovieOverviewTranslation2();
        fi.overview = "overview FI";
        fi.ISO_639_1 = "fi";
        fi.the_movie_db_id = 1;

        TheMovieDBMovieOverviewTranslation2 en = new TheMovieDBMovieOverviewTranslation2();
        en.overview = "overview EN";
        en.ISO_639_1 = "en";
        en.the_movie_db_id = 1;

        theMovieDBMovieOverviewTranslationResource.save(fi);
        theMovieDBMovieOverviewTranslationResource.save(en);
        TheMovieDBMovieOverviewTranslation2 fi1 = theMovieDBMovieOverviewTranslationResource.findByMovieDBIdAndLanguage(1, "fi");
        TheMovieDBMovieOverviewTranslation2 en1 = theMovieDBMovieOverviewTranslationResource.findByMovieDBIdAndLanguage(1, "en");
        Assert.assertNotNull(fi1);
        Assert.assertNotNull(en1);
    }

    @Test
    public void testFindMovieByMovieDbIdWithOverview(){
        theMovieDBMovieResource.deleteAll();
        theMovieDBMovieOverviewTranslationResource.deleteAll();

        int theMovieDbId = 111;
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId);
        theMovieDBMovieResource.save(theMovieDBMovie2);

        TheMovieDBMovieOverviewTranslation2 fi = new TheMovieDBMovieOverviewTranslation2();
        fi.overview = "overview FI";
        fi.ISO_639_1 = "fi";
        fi.the_movie_db_id = 111;

        TheMovieDBMovieOverviewTranslation2 en = new TheMovieDBMovieOverviewTranslation2();
        en.overview = "overview EN";
        en.ISO_639_1 = "en";
        en.the_movie_db_id = 111;

        theMovieDBMovieOverviewTranslationResource.save(fi);
        theMovieDBMovieOverviewTranslationResource.save(en);

        TheMovieDBMovie2 find = theMovieDBMovieResource.findMovieByMovieDbIdWithOverview(111);
        Assert.assertNotNull(find);
        Assert.assertNotNull(find.theMovieDBMovieOverviewTranslation2);
        Assert.assertFalse(find.theMovieDBMovieOverviewTranslation2.isEmpty());
        Assert.assertTrue(find.theMovieDBMovieOverviewTranslation2.size() == 2);
    }

    @Test
    public void tesSaveAndFindMovieByMovieDbIdWithReleaseDate(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieReleaseDateResource.deleteAll();
        theMovieDBMovieResource.deleteAll();

        int theMovieDbId = 111;
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId);//1
        theMovieDBMovieResource.save(theMovieDBMovie2);

        TheMovieDBMovie2 theMovieDBMovie22 = TheMovieDBServiceTest.dummy(1221);//2
        theMovieDBMovieResource.save(theMovieDBMovie22);

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate2 = new TheMovieDBMovieReleaseDate2();//1
        theMovieDBMovieReleaseDate2.the_movie_db_id = theMovieDbId;
        theMovieDBMovieReleaseDate2.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate2.type = 1;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate22 = new TheMovieDBMovieReleaseDate2();//1
        theMovieDBMovieReleaseDate22.the_movie_db_id = theMovieDbId;
        theMovieDBMovieReleaseDate22.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate22.type = 2;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate33 = new TheMovieDBMovieReleaseDate2();//2
        theMovieDBMovieReleaseDate33.the_movie_db_id = 1221;
        theMovieDBMovieReleaseDate33.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate33.type = 1;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate44 = new TheMovieDBMovieReleaseDate2();//2
        theMovieDBMovieReleaseDate44.the_movie_db_id = 1221;
        theMovieDBMovieReleaseDate44.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate44.type = 2;

        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate2);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate22);
        Assert.assertTrue(theMovieDBMovieResource.findMovieByReleaseTypeWithReleaseDate(1).size() == 1);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate33);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate44);
        Assert.assertTrue(theMovieDBMovieResource.findMovieByReleaseTypeWithReleaseDate(1).size() == 2);
        List<TheMovieDBMovie2> movieByReleaseTypeWithReleaseDate = theMovieDBMovieResource.findMovieByReleaseTypeWithReleaseDate(1);
        Assert.assertNotNull(movieByReleaseTypeWithReleaseDate);
        TheMovieDBMovie2 movie1 = movieByReleaseTypeWithReleaseDate.get(0);
        TheMovieDBMovie2 movie2 = movieByReleaseTypeWithReleaseDate.get(1);
        Assert.assertNotNull(movie1);
        Assert.assertNotNull(movie2);
        Set<TheMovieDBMovieReleaseDate2> releaseDate2s = movie1.releaseDate2s;
        Assert.assertNotNull(releaseDate2s);
        Assert.assertTrue(releaseDate2s.size() == 1);
        Set<TheMovieDBMovieReleaseDate2> releaseDate22s = movie2.releaseDate2s;
        Assert.assertNotNull(releaseDate22s);
        Assert.assertTrue(releaseDate2s.size() == 1);
    }


    @Test
    public void tesSaveAndFindMovieByMovieDbIdWithReleaseDateAndOverview(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieReleaseDateResource.deleteAll();
        theMovieDBMovieResource.deleteAll();

        int theMovieDbId = 111;
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId);//1
        theMovieDBMovieResource.save(theMovieDBMovie2);

        TheMovieDBMovie2 theMovieDBMovie22 = TheMovieDBServiceTest.dummy(1221);//2
        theMovieDBMovieResource.save(theMovieDBMovie22);

        TheMovieDBMovieOverviewTranslation2 fiOverview = new TheMovieDBMovieOverviewTranslation2();
        fiOverview.the_movie_db_id = theMovieDbId;
        fiOverview.ISO_639_1 = "fi";
        fiOverview.overview = "FI OVERVIEW";
        theMovieDBMovieOverviewTranslationResource.save(fiOverview);

        TheMovieDBMovieOverviewTranslation2 enOverview = new TheMovieDBMovieOverviewTranslation2();
        enOverview.the_movie_db_id = theMovieDbId;
        enOverview.ISO_639_1 = "en";
        enOverview.overview = "EN OVERVIEW";
        theMovieDBMovieOverviewTranslationResource.save(enOverview);

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate2 = new TheMovieDBMovieReleaseDate2();//1
        theMovieDBMovieReleaseDate2.the_movie_db_id = theMovieDbId;
        theMovieDBMovieReleaseDate2.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate2.type = 1;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate22 = new TheMovieDBMovieReleaseDate2();//1
        theMovieDBMovieReleaseDate22.the_movie_db_id = theMovieDbId;
        theMovieDBMovieReleaseDate22.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate22.type = 2;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate33 = new TheMovieDBMovieReleaseDate2();//2
        theMovieDBMovieReleaseDate33.the_movie_db_id = 1221;
        theMovieDBMovieReleaseDate33.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate33.type = 1;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate44 = new TheMovieDBMovieReleaseDate2();//2
        theMovieDBMovieReleaseDate44.the_movie_db_id = 1221;
        theMovieDBMovieReleaseDate44.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate44.type = 2;

        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate2);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate22);
        Assert.assertTrue(theMovieDBMovieResource.findMovieByReleaseTypeWithReleaseDate(1).size() == 1);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate33);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate44);
        Assert.assertTrue(theMovieDBMovieResource.findMovieByReleaseTypeWithReleaseDate(1).size() == 2);
        List<TheMovieDBMovie2> movieByReleaseTypeWithReleaseDate = theMovieDBMovieResource.findMovieByReleaseTypeWithReleaseDateAndOverview(1);
        Assert.assertNotNull(movieByReleaseTypeWithReleaseDate);
        TheMovieDBMovie2 movie1 = movieByReleaseTypeWithReleaseDate.get(0);
        TheMovieDBMovie2 movie2 = movieByReleaseTypeWithReleaseDate.get(1);
        Set<TheMovieDBMovieOverviewTranslation2> theMovieDBMovieOverviewTranslation2 = movie1.theMovieDBMovieOverviewTranslation2;
        Assert.assertFalse(theMovieDBMovieOverviewTranslation2.isEmpty());
        Assert.assertNotNull(movie1);
        Assert.assertNotNull(movie2);
        Set<TheMovieDBMovieReleaseDate2> releaseDate2s = movie1.releaseDate2s;
        Assert.assertNotNull(releaseDate2s);
        Assert.assertTrue(releaseDate2s.size() == 1);
        Set<TheMovieDBMovieReleaseDate2> releaseDate22s = movie2.releaseDate2s;
        Assert.assertNotNull(releaseDate22s);
        Assert.assertTrue(releaseDate2s.size() == 1);
    }

    @Test
    public void tesSaveAndFindMovieByMovieDbIdWithReleaseDateAndOverviewAndByLang(){
        theMovieDBMovieOverviewTranslationResource.deleteAll();
        theMovieDBMovieReleaseDateResource.deleteAll();
        theMovieDBMovieResource.deleteAll();

        int theMovieDbId = 111;
        TheMovieDBMovie2 theMovieDBMovie2 = TheMovieDBServiceTest.dummy(theMovieDbId);//1
        theMovieDBMovieResource.save(theMovieDBMovie2);

        TheMovieDBMovie2 theMovieDBMovie22 = TheMovieDBServiceTest.dummy(1221);//2
        theMovieDBMovieResource.save(theMovieDBMovie22);

        TheMovieDBMovieOverviewTranslation2 fiOverview = new TheMovieDBMovieOverviewTranslation2();
        fiOverview.the_movie_db_id = theMovieDbId;
        fiOverview.ISO_639_1 = "fi";
        fiOverview.overview = "FI OVERVIEW";
        theMovieDBMovieOverviewTranslationResource.save(fiOverview);

        TheMovieDBMovieOverviewTranslation2 enOverview = new TheMovieDBMovieOverviewTranslation2();
        enOverview.the_movie_db_id = theMovieDbId;
        enOverview.ISO_639_1 = "en";
        enOverview.overview = "EN OVERVIEW";
        theMovieDBMovieOverviewTranslationResource.save(enOverview);

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate2 = new TheMovieDBMovieReleaseDate2();//1
        theMovieDBMovieReleaseDate2.the_movie_db_id = theMovieDbId;
        theMovieDBMovieReleaseDate2.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate2.type = 1;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate22 = new TheMovieDBMovieReleaseDate2();//1
        theMovieDBMovieReleaseDate22.the_movie_db_id = theMovieDbId;
        theMovieDBMovieReleaseDate22.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate22.type = 2;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate33 = new TheMovieDBMovieReleaseDate2();//2
        theMovieDBMovieReleaseDate33.the_movie_db_id = 1221;
        theMovieDBMovieReleaseDate33.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate33.type = 1;

        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate44 = new TheMovieDBMovieReleaseDate2();//2
        theMovieDBMovieReleaseDate44.the_movie_db_id = 1221;
        theMovieDBMovieReleaseDate44.release_date = new Date(System.currentTimeMillis());
        theMovieDBMovieReleaseDate44.type = 2;

        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate2);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate22);
        Assert.assertTrue(theMovieDBMovieResource.findMovieByReleaseTypeWithReleaseDate(1).size() == 1);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate33);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate44);
        Assert.assertTrue(theMovieDBMovieResource.findMovieByReleaseTypeWithReleaseDate(1).size() == 2);
        List<TheMovieDBMovie2> movieByReleaseTypeWithReleaseDate = theMovieDBMovieResource.findMovieByReleaseTypeAndLangWithReleaseDateAndOverview(1, "fi");
        Assert.assertNotNull(movieByReleaseTypeWithReleaseDate);
        TheMovieDBMovie2 movie1 = movieByReleaseTypeWithReleaseDate.get(0);
        Set<TheMovieDBMovieOverviewTranslation2> theMovieDBMovieOverviewTranslation2 = movie1.theMovieDBMovieOverviewTranslation2;
        Assert.assertFalse(theMovieDBMovieOverviewTranslation2.isEmpty());
        TheMovieDBMovieOverviewTranslation2 next = theMovieDBMovieOverviewTranslation2.iterator().next();
        Assert.assertTrue(next.ISO_639_1.equals("fi"));
        Assert.assertNotNull(movie1);
        Set<TheMovieDBMovieReleaseDate2> releaseDate2s = movie1.releaseDate2s;
        Assert.assertNotNull(releaseDate2s);
        Assert.assertTrue(releaseDate2s.size() == 1);
        Assert.assertTrue(releaseDate2s.size() == 1);
    }
}