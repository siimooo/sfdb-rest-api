package sfdb.resource.themoviedb;

import io.sfdb.SFDbApplication;
import io.sfdb.resource.themoviedb.TheMovieDBMovieReleaseDateResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieReleaseDate2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * User: Simo Ala-Kotila
 * Date: 25/04/2017
 * Time: 14.58
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class TheMovieDbReleaseDateResourceTest {

    @Autowired
    private TheMovieDBMovieReleaseDateResource theMovieDBMovieReleaseDateResource;

    @Before
    public void initData(){
        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate1 = new TheMovieDBMovieReleaseDate2();
        theMovieDBMovieReleaseDate1.the_movie_db_id = 1111111;
        theMovieDBMovieReleaseDate1.release_date = new Date(System.currentTimeMillis() - 3141340);
        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate2 = new TheMovieDBMovieReleaseDate2();
        theMovieDBMovieReleaseDate2.the_movie_db_id = 2222222;
        theMovieDBMovieReleaseDate2.release_date = new Date(System.currentTimeMillis() - 5500);
        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate3 = new TheMovieDBMovieReleaseDate2();
        theMovieDBMovieReleaseDate3.the_movie_db_id = 3333333;
        theMovieDBMovieReleaseDate3.release_date = new Date(System.currentTimeMillis() - 1124000);
        TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate4 = new TheMovieDBMovieReleaseDate2();
        theMovieDBMovieReleaseDate4.the_movie_db_id = 4444444;
        theMovieDBMovieReleaseDate3.release_date = new Date(System.currentTimeMillis() - 11350000);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate1);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate2);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate3);
        theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate4);
    }

    @Test
    public void testFindAllOrderByDesc(){
        List<TheMovieDBMovieReleaseDate2> allOrderByDesc = theMovieDBMovieReleaseDateResource.findAllOrderByDesc();
        for (TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate2 : allOrderByDesc) {
            System.out.println(theMovieDBMovieReleaseDate2.the_movie_db_id);
        }
        Assert.assertNotNull(allOrderByDesc);
        Assert.assertFalse(allOrderByDesc.isEmpty());
        Assert.assertTrue(allOrderByDesc.get(0).the_movie_db_id == 2222222);
        Assert.assertTrue(allOrderByDesc.get(3).the_movie_db_id == 4444444);
    }

}
