package sfdb.resource.omdb;

import io.rxjavaomdb.network.OMDbClientImpl;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;
import io.sfdb.SFDbApplication;
import io.sfdb.resource.omdb.OMDbMovieResource;
import io.sfdb.resource.omdb.model.OMDbMovie;
import io.sfdb.service.omdb.OMDbService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import rx.Subscriber;

import java.util.Date;

/**
 * User: Simo Ala-Kotila
 * Date: 09/03/17
 * Time: 07:43
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SFDbApplication.class)
public class OMDbResourceTest {

    @Autowired
    private OMDbMovieResource omDbMovieResource;

    @Test
    public void testSaveAndFindByImdbId(){
        String imdbId = "tt0944947";
        Date released = new Date(System.currentTimeMillis());
        String title = "aaa";
        String posterPath = "aa";
        String plot = "aa";
        omDbMovieResource.save(new OMDbMovie(imdbId, released, title, posterPath, plot));
        OMDbMovie byImdbId = omDbMovieResource.findByImdbId(imdbId);
        Assert.assertNotNull(byImdbId);
    }
}