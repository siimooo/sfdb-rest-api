package sfdb;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * User: Simo Ala-Kotila
 * Date: 05/11/16
 * Time: 19:29
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class TestConfig {

//    @Bean
//    public HelloService helloServiceImpl(){
//        return new HelloServiceImpl(text());
//    }
//
//    @Bean
//    public String text(){
//        return "testForText";
//    }
}