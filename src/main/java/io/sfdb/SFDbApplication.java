package io.sfdb;

import io.rxjavaomdb.network.OMDbClient;
import io.rxjavaomdb.network.OMDbClientImpl;
import io.rxjavathemoviedb.network.genre.TheMovieDBGenreClient;
import io.rxjavathemoviedb.network.genre.TheMovieDBGenreClientImpl;
import io.rxjavathemoviedb.network.movies.TheMovieDBClient;
import io.rxjavathemoviedb.network.movies.TheMovieDBClientImpl;
import io.sfdb.resource.omdb.OMDbMovieResource;
import io.sfdb.resource.themoviedb.*;
import io.sfdb.service.omdb.OMDbService;
import io.sfdb.service.omdb.impl.OMDbServiceImpl;
import io.sfdb.service.sfdb.SFDbMovieService;
import io.sfdb.service.sfdb.impl.SFDbMovieServiceImpl;
import io.sfdb.service.themoviedb.TheMovieDBMovieService;
import io.sfdb.service.themoviedb.impl.TheMovieDBCollectionV2Impl;
import io.sfdb.service.themoviedb.impl.TheMovieDBGenreServiceImpl;
import io.sfdb.service.themoviedb.impl.TheMovieDBMovieServiceImpl;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.Slf4jRequestLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.jetty.JettyEmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.jetty.JettyServerCustomizer;
import org.springframework.context.annotation.Bean;


/**
 * User: Simo Ala-Kotila
 * Date: 27/12/16
 * Time: 13:23
 */
@SpringBootApplication
public class SFDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SFDbApplication.class, args);
    }

    @Autowired
    private TheMovieDBMovieResource theMovieDBMovieResource;

    @Autowired
    private TheMovieDBMovieGenreListResource theMovieDBMovieGenreListResource;

    @Autowired
    private TheMovieDBCollectionResource theMovieDBCollection;

    @Autowired
    private TheMovieDBMovieGenreResource theMovieDBMovieGenreResource;

    @Autowired
    private OMDbMovieResource omDbMovieResource;

    @Autowired
    private TheMovieDBMovieGenreListTranslationResource theMovieDBMovieGenreListTranslationResource;

    @Autowired
    private TheMovieDBMovieReleaseDateResource theMovieDBMovieReleaseDateResource;

    @Autowired
    private TheMovieDBCollectionTranslationResource theMovieDBCollectionTranslationResource2;

    @Autowired
    private TheMovieDBMovieOverviewTranslationResource theMovieDBMovieOverviewTranslationResource;

    @Bean
    public TheMovieDBCollectionV2Impl createTheMovieDBCollectionV2(){
        return new TheMovieDBCollectionV2Impl(theMovieDBCollectionTranslationResource2, theMovieDBCollection);
    }

    @Bean
    public TheMovieDBMovieService createTheMovieDBMovieService(){
        return new TheMovieDBMovieServiceImpl(createTheMovieDBClient(), theMovieDBMovieResource, createTheMovieDBCollectionV2(), theMovieDBMovieOverviewTranslationResource, theMovieDBMovieReleaseDateResource);
    }

    @Bean
    public OMDbService creaOmDbService(){
        return new OMDbServiceImpl(createOmDbClient(), omDbMovieResource, theMovieDBMovieResource);
    }

    @Bean
    public TheMovieDBGenreServiceImpl createTheMovieDBGenreService(){
        return new TheMovieDBGenreServiceImpl(new TheMovieDBGenreClientImpl(), theMovieDBMovieGenreListResource, theMovieDBMovieGenreResource, theMovieDBMovieGenreListTranslationResource);
    }

    @Bean
    public SFDbMovieService createSfDbMovieService(){
        return new SFDbMovieServiceImpl(theMovieDBMovieResource, omDbMovieResource);
    }

    /** enable request/response logging **/
    @Bean
    public JettyEmbeddedServletContainerFactory jettyEmbeddedServletContainerFactory(@Value("${server.port:8080}") final String mainPort ) {
        final JettyEmbeddedServletContainerFactory factory = new JettyEmbeddedServletContainerFactory( Integer.valueOf( mainPort ) );

        factory.addServerCustomizers(new JettyServerCustomizer() {
            @Override
            public void customize(Server server) {
                server.setRequestLog(new Slf4jRequestLog());
            }
        });
        return factory;
    }

    @Bean
    public OMDbClient createOmDbClient(){
        return new OMDbClientImpl();
    }

    @Bean
    public TheMovieDBClient createTheMovieDBClient(){
        return new TheMovieDBClientImpl();
    }

    @Bean
    public TheMovieDBGenreClient createTheMovieDBGenreClient(){
        return new TheMovieDBGenreClientImpl();
    }
}