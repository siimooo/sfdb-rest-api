package io.sfdb.resource.omdb;

import io.sfdb.resource.omdb.model.OMDbMovie;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * User: Simo Ala-Kotila
 * Date: 06/01/17
 * Time: 13:54
 */
@Transactional
public interface OMDbMovieResource extends CrudRepository<OMDbMovie, Integer> {

    OMDbMovie findByImdbId(String imdbId);
}
