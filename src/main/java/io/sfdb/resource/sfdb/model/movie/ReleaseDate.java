package io.sfdb.resource.sfdb.model.movie;

import java.util.Date;

/**
 * User: Simo Ala-Kotila
 * Date: 10/03/17
 * Time: 17:53
 */
public class ReleaseDate {

    public Date releaseDate;
    public Type type;
    public String ISO_639_1;

    public enum Type {

        PREMIERE(1),
        THEATRICAL_LIMITED(2),
        THEATRICAL(3),
        DIGITAL(4),
        PHYSICAL(5),
        TV(6),
        UNKNOWN(666),
        NOT_SET(7);

        public final int code;

        Type(int code){
            this.code = code;
        }

        public static Type fromCode(Integer code) {
            if(code == null){
                return NOT_SET;
            }
            for (Type typе : Type.values()) {
                if (typе.code == code) {
                    return typе;
                }
            }
            return UNKNOWN;
        }

    }

}
