package io.sfdb.resource.sfdb.model.movie;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Simo Ala-Kotila
 * Date: 10/03/17
 * Time: 17:55
 */
public class Movie {

    public String title;
    public String originalTitle;
    public String poster;
    public String plot;
    public String imdbId;
    public int movie_db_id;
    public String lang;

    public List<Rating> ratings = new ArrayList<>();
    public List<Genre> genres = new ArrayList<>();
    public List<ReleaseDate> releaseDates = new ArrayList<>();
}