package io.sfdb.resource.themoviedb;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieGenre2;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * User: Simo Ala-Kotila
 * Date: 21/02/17
 * Time: 10:35
 */
@Transactional
public interface TheMovieDBMovieGenreResource extends CrudRepository<TheMovieDBMovieGenre2, Integer> {

    List<TheMovieDBMovieGenre2> findGenresByTheMovieDbId(int theMovieDbId);
}