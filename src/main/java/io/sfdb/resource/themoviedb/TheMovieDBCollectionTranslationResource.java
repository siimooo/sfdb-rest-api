package io.sfdb.resource.themoviedb;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBCollectionTranslation2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

/**
 * User: Simo Ala-Kotila
 * Date: 17/02/17
 * Time: 10:01
 */
@Transactional
public interface TheMovieDBCollectionTranslationResource extends PagingAndSortingRepository<TheMovieDBCollectionTranslation2, Integer> {

    @Query("SELECT m FROM TheMovieDBCollectionTranslation2 m where m.collectionId = ?1 AND m.ISO_639_1 = ?2")
    TheMovieDBCollectionTranslation2 findByCollectionIdIdAndLanguage(Integer collectionId, String lang);
}