package io.sfdb.resource.themoviedb;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieOverviewTranslation2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieReleaseDate2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**§
 * User: Simo Ala-Kotila
 * Date: 17/04/17
 * Time: 11:47
 */
@Transactional
public interface TheMovieDBMovieReleaseDateResource extends CrudRepository<TheMovieDBMovieReleaseDate2, Integer> {

    @Query("SELECT m FROM TheMovieDBMovieReleaseDate2 m where m.the_movie_db_id = ?1 AND m.ISO_639_1 = ?2 AND m.type = ?3")
    TheMovieDBMovieReleaseDate2 findByMovieDBIdAndLanguageAndType(Integer the_movie_db_id, String lang, int type);

    @Query("SELECT m FROM TheMovieDBMovieReleaseDate2 m where m.type = ?1")
    List<TheMovieDBMovieReleaseDate2> findByType(int type);

    @Query("SELECT m FROM TheMovieDBMovieReleaseDate2 m ORDER BY m.release_date DESC")
    List<TheMovieDBMovieReleaseDate2> findAllOrderByDesc();

}
