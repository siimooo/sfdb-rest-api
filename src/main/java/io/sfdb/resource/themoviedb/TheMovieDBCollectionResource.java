package io.sfdb.resource.themoviedb;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBCollection2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * User: Simo Ala-Kotila
 * Date: 17/02/17
 * Time: 10:01
 */
@Transactional
public interface TheMovieDBCollectionResource extends CrudRepository<TheMovieDBCollection2, Integer>{

    TheMovieDBCollection2 findByCollectionId(int collectionId);

    @Query("SELECT m FROM TheMovieDBCollection2 m JOIN FETCH m.theMovieDBCollectionTranslation2s t WHERE t.collectionId=?1")
    TheMovieDBCollection2 findByCollectionIdWithTranslation(int collectionId);
}