package io.sfdb.resource.themoviedb;

import io.rxjavathemoviedb.network.moshi.TheMovieDBGenre;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.sfdb.service.LocaleHelper;
import io.sfdb.service.omdb.OMDbService;
import io.sfdb.service.themoviedb.TheMovieDBCollectionServiceV2;
import io.sfdb.service.themoviedb.TheMovieDBGenreService;
import io.sfdb.service.themoviedb.TheMovieDBMovieService;
import io.sfdb.service.themoviedb.impl.TheMovieDBGenreServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User: Simo Ala-Kotila
 * Date: 16/01/17
 * Time: 17:27
 */
@Service
public class MovieInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(TheMovieDBGenreServiceImpl.class);

    @Autowired
    private TheMovieDBMovieService theMovieDBMovieService;

    @Autowired
    private TheMovieDBGenreService theMovieDBGenreService;

    @Autowired
    private TheMovieDBCollectionServiceV2 theMovieDBCollectionService;

    @Autowired
    private OMDbService omDbService;

    @EventListener
    public void init(ApplicationReadyEvent event) {
        LOG.info("init will start for the MovieInitializer..");
        if(theMovieDBGenreService.isGenresEmpty()){//genres
            LOG.info("init will start for the theMovieDBGenreService..");
            LOG.info("query genres...");
            List<TheMovieDBGenre> theMovieDBGenres = theMovieDBGenreService.genres();//query from API
            LOG.info("query genres done...");
            LOG.info("query genres FI...");
            List<TheMovieDBGenre> theMovieDBGenresFI = theMovieDBGenreService.genres(LocaleHelper.fi());//query from API
            LOG.info("query genres FI done...");
            LOG.info("save genres...");
            theMovieDBGenreService.saveGenreListTranslations(theMovieDBGenres);
            LOG.info("save genres FI...");
            theMovieDBGenreService.saveGenreListTranslations(theMovieDBGenresFI, LocaleHelper.fi());
            LOG.info("save genres FI done...");
            LOG.info("init theMovieDBGenreService is now done");
        }else{
            LOG.info("theMovieDBGenreService init is not needed to execute..");
        }
        if(theMovieDBMovieService.isMovieDetailEmpty()){//upcomings
            LOG.info("init will start for the theMovieDBMovieService and omDbService..");
            List<TheMovieDBMovieUpcomingResults> theMovieDBMovieUpcomingResultses = theMovieDBMovieService.upcomings();//query from API
            LOG.info("save upcoming...");
            theMovieDBMovieService.saveUpcomings(theMovieDBMovieUpcomingResultses);
            LOG.info("save movie genres...");
            theMovieDBGenreService.saveMovieGenres(theMovieDBMovieUpcomingResultses);
            LOG.info("save release dates...");
            theMovieDBMovieService.saveReleaseDates(theMovieDBMovieUpcomingResultses);
            LOG.info("save overview FI...");
            theMovieDBMovieService.saveOverview(theMovieDBMovieUpcomingResultses, LocaleHelper.fi());
//            theMovieDBCollectionService.saveCollectionTranslation(1, LocaleHelper.fi());//TODO: continue from HERE
            LOG.info("init theMovieDBMovieService and omdbService is now done");
            LOG.info("init will start for the omDbService..");
            omDbService.saveMovies(theMovieDBMovieUpcomingResultses);
        }else{
            LOG.info("theMovieDBMovieService init is not needed to execute..");
        }
        LOG.info("init is done for the MovieInitializer..");
    }
}