package io.sfdb.resource.themoviedb.model.movie;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User: Simo Ala-Kotila
 * Date: 20/02/17
 * Time: 17:24
 */
@Entity
@Table(name = "the_movie_db_movie_genre")
public class TheMovieDBMovieGenre2 implements Serializable {

    private static final long serialVersionUID = 632096425018734151L;

    public TheMovieDBMovieGenre2(){}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;
    public Integer genre_id;
    @Column(name ="the_movie_db_id")
    public int theMovieDbId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "genre_id", referencedColumnName = "genre_id", insertable = false, updatable = false)
    public TheMovieDBGenreList2 theMovieDBGenreList;
}
