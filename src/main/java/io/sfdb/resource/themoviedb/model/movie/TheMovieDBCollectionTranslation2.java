package io.sfdb.resource.themoviedb.model.movie;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: Simo Ala-Kotila
 * Date: 17/02/17
 * Time: 10:02
 */
@Entity
@Table(name = "the_movie_db_collection_translation")
public class TheMovieDBCollectionTranslation2 implements Serializable{

    private static final long serialVersionUID = 6471629960321349282L;

    public TheMovieDBCollectionTranslation2(){}

    // An autogenerated id (unique for each user in the db)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;
    @Size(max = 500, message = "name max length is 500")
    public String name;
    @Column(name ="collection_id")
    public Integer collectionId;
    @Size(max = 2, message = "max size for the ISO_639_1 is 2")
    public String ISO_639_1;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name="collection_id", referencedColumnName = "collection_id", updatable = false, insertable = false)
    public TheMovieDBCollection2 theMovieDBCollection;
}