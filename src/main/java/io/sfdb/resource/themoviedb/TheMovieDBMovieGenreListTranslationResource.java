package io.sfdb.resource.themoviedb;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreList2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreListTranslation2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

/**
 * User: Simo Ala-Kotila
 * Date: 13/03/17
 * Time: 12:01
 */
@Transactional
public interface TheMovieDBMovieGenreListTranslationResource extends PagingAndSortingRepository<TheMovieDBGenreListTranslation2, Integer> {

    @Query("SELECT m FROM TheMovieDBGenreListTranslation2 m where m.genreId = ?1 AND m.ISO_639_1 = ?2")
    TheMovieDBGenreListTranslation2 findByGenreIdAndLanguage(Integer genreId, String lang);
}