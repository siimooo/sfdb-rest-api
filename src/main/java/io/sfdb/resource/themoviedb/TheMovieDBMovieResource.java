package io.sfdb.resource.themoviedb;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * User: Simo Ala-Kotila
 * Date: 06/01/17
 * Time: 13:52
 */
@Transactional
public interface TheMovieDBMovieResource extends PagingAndSortingRepository<TheMovieDBMovie2, Integer> {

    TheMovieDBMovie2 findByTheMovieDbId(int theMovieDbId);

    @Query("SELECT m FROM TheMovieDBMovie2 m where m.theMovieDbId = ?1 AND m.original_language = ?2")
    List<TheMovieDBMovie2> findByTheMovieDbIdAndOriginalLanguage(int theMovieDbId, String original_language);

    @Query("SELECT m FROM TheMovieDBMovie2 m where m.original_language = ?1")
    List<TheMovieDBMovie2> findByOriginalLanguage(String original_language);

    @Query("SELECT m FROM TheMovieDBMovie2 m JOIN FETCH m.theMovieDBMovieOverviewTranslation2 t WHERE t.the_movie_db_id=?1")
    TheMovieDBMovie2 findMovieByMovieDbIdWithOverview(int theMovieDbId);

    @Query("SELECT m FROM TheMovieDBMovie2 m JOIN FETCH m.releaseDate2s t WHERE t.the_movie_db_id=m.theMovieDbId AND t.type = ?1 ORDER BY t.release_date DESC")
    List<TheMovieDBMovie2> findMovieByReleaseTypeWithReleaseDate(int type);

    @Query("SELECT m FROM TheMovieDBMovie2 m JOIN FETCH m.releaseDate2s t WHERE t.the_movie_db_id=m.theMovieDbId AND t.type = ?1 ORDER BY t.release_date DESC")
    List<TheMovieDBMovie2> findMovieByReleaseTypeWithReleaseDate(int type, Pageable pageable);

    @Query("SELECT m FROM TheMovieDBMovie2 m JOIN FETCH m.releaseDate2s t JOIN FETCH m.theMovieDBMovieOverviewTranslation2 o WHERE t.the_movie_db_id=m.theMovieDbId AND o.the_movie_db_id=m.theMovieDbId AND t.type = ?1 ORDER BY t.release_date DESC")
    List<TheMovieDBMovie2> findMovieByReleaseTypeWithReleaseDateAndOverview(int type);

    @Query("SELECT m FROM TheMovieDBMovie2 m JOIN FETCH m.releaseDate2s t JOIN FETCH m.theMovieDBMovieOverviewTranslation2 o WHERE t.the_movie_db_id=m.theMovieDbId AND o.the_movie_db_id=m.theMovieDbId AND t.type = ?1 ORDER BY t.release_date DESC")
    List<TheMovieDBMovie2> findMovieByReleaseTypeWithReleaseDateAndOverview(int type, Pageable pageable);

    @Query("SELECT m FROM TheMovieDBMovie2 m JOIN FETCH m.releaseDate2s t JOIN FETCH m.theMovieDBMovieOverviewTranslation2 o WHERE t.the_movie_db_id=m.theMovieDbId AND o.the_movie_db_id=m.theMovieDbId AND t.type = ?1 AND o.ISO_639_1=?2 ORDER BY t.release_date DESC")
    List<TheMovieDBMovie2> findMovieByReleaseTypeAndLangWithReleaseDateAndOverview(int type, String lang);

    @Query("SELECT m FROM TheMovieDBMovie2 m JOIN FETCH m.releaseDate2s t JOIN FETCH m.theMovieDBMovieOverviewTranslation2 o WHERE t.the_movie_db_id=m.theMovieDbId AND o.the_movie_db_id=m.theMovieDbId AND t.type = ?1 AND o.ISO_639_1=?2 ORDER BY t.release_date DESC")
    List<TheMovieDBMovie2> findMovieByReleaseTypeAndLangWithReleaseDateAndOverview(int type, String lang, Pageable pageable);
}
