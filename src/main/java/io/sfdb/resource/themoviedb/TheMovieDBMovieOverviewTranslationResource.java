package io.sfdb.resource.themoviedb;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreListTranslation2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieGenre2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieOverviewTranslation2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * User: Simo Ala-Kotila
 * Date: 10/04/17
 * Time: 16:33
 */
@Transactional
public interface TheMovieDBMovieOverviewTranslationResource extends CrudRepository<TheMovieDBMovieOverviewTranslation2, Integer> {

    @Query("SELECT m FROM TheMovieDBMovieOverviewTranslation2 m where m.the_movie_db_id = ?1 AND m.ISO_639_1 = ?2")
    TheMovieDBMovieOverviewTranslation2 findByMovieDBIdAndLanguage(Integer the_movie_db_id, String lang);
}
