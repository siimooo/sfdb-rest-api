package io.sfdb.resource.themoviedb;

import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreList2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

/**
 * User: Simo Ala-Kotila
 * Date: 20/01/17
 * Time: 09:17
 */
@Transactional
public interface TheMovieDBMovieGenreListResource extends PagingAndSortingRepository<TheMovieDBGenreList2, Integer> {

    TheMovieDBGenreList2 findTheMovieGenreByGenreId(int genreId);

    @Query("SELECT g FROM TheMovieDBGenreList2 g JOIN FETCH g.theMovieDBGenreListTranslation2s t WHERE t.genreId=?1")
    TheMovieDBGenreList2 findGenreByGenreIdWithTranslastion(int genreId);
}