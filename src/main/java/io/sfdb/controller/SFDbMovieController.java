package io.sfdb.controller;

import com.google.common.base.Strings;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBUpcoming;
import io.sfdb.resource.sfdb.model.movie.Movie;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import io.sfdb.service.LocaleHelper;
import io.sfdb.service.omdb.OMDbService;
import io.sfdb.service.sfdb.SFDbMovieService;
import io.sfdb.service.themoviedb.TheMovieDBGenreService;
import io.sfdb.service.themoviedb.TheMovieDBMovieService;
import io.sfdb.service.themoviedb.impl.TheMovieDBMovieServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Simo Ala-Kotila
 * Date: 17/01/1711
 * Time: 09:02
 */
@RestController
public class SFDbMovieController {

    private static final Logger LOG = LoggerFactory.getLogger(SFDbMovieController.class);

    @Autowired
    private TheMovieDBMovieService theMovieDBMovieService;

    @Autowired
    private TheMovieDBMovieResource theMovieDBMovieResource;

    @Autowired
    private TheMovieDBGenreService theMovieDBGenreService;

    @Autowired
    private OMDbService omDbService;

    @Autowired
    private SFDbMovieService sfDbMovieService;

    @RequestMapping(path = "/v1/upcomings/{index}", method = RequestMethod.GET)
    public ResponseEntity<TheMovieDBUpcoming> upcomingsByIndexAndType(@PathVariable("index") Integer index,
                                                                      @RequestParam(value = "type", required = false) String type){
        //return all upcomings

        return new ResponseEntity<TheMovieDBUpcoming>(theMovieDBMovieService.upcoming(index), HttpStatus.OK);
    }

    @RequestMapping(path = "/v1/movies/newReleases/{index}", method = RequestMethod.GET)
    public ResponseEntity<List<Movie>> getMovies(@PathVariable("index") Integer index,
                                                 @RequestParam(value = "lang", required = false, defaultValue = "en") String lang){
        List<Movie> movies = sfDbMovieService.newReleases(index, lang);
        return new ResponseEntity<List<Movie>>(movies, HttpStatus.OK);
    }

    @RequestMapping(path = "/v1/movies/upcoming", method = RequestMethod.POST)
    public ResponseEntity<String> newMoviesUpComings(){
        LOG.info("query new upcoming movies");
        List<TheMovieDBMovieUpcomingResults> theMovieDBMovieUpcomingResultses = theMovieDBMovieService.upcomings();
        theMovieDBMovieService.saveUpcomings(theMovieDBMovieUpcomingResultses);
        theMovieDBGenreService.saveMovieGenres(theMovieDBMovieUpcomingResultses);
        theMovieDBMovieService.saveReleaseDates(theMovieDBMovieUpcomingResultses);
        theMovieDBMovieService.saveOverview(theMovieDBMovieUpcomingResultses, LocaleHelper.fi());
        omDbService.saveMovies(theMovieDBMovieUpcomingResultses);
        return new ResponseEntity<String>("updated", HttpStatus.OK);
    }

    private boolean isOriginalLanguage(String originalLanguage) {
        return !Strings.isNullOrEmpty(originalLanguage);
    }
}