package io.sfdb.service;

import java.util.Locale;

/**
 * User: Simo Ala-Kotila
 * Date: 26/03/17
 * Time: 14:57
 */
public class LocaleHelper {

    private LocaleHelper(){}

    public static Locale fi(){
        return new Locale("fi", "FI");
    }
}
