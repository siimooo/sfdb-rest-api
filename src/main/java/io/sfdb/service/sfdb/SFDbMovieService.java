package io.sfdb.service.sfdb;

import io.sfdb.resource.sfdb.model.movie.Movie;

import java.util.List;

/**
 *
 * User: Simo Ala-Kotila
 * Date: 07/01/17
 * Time: 12:53
 */
public interface SFDbMovieService {

    void comingSoonInTheatres();

    void newReleasesInTheatres();

    void newReleasesPhysicalAndDigital();

    void comingSoonPhysicalAndDigital();

    List<Movie> newReleases(int index, String lang);

    void comingSoon();

    List<Movie> all(int index);
}
