package io.sfdb.service.sfdb.impl;

import com.google.common.base.Strings;
import io.sfdb.resource.omdb.OMDbMovieResource;
import io.sfdb.resource.omdb.model.OMDbMovie;
import io.sfdb.resource.sfdb.model.movie.Movie;
import io.sfdb.resource.sfdb.model.movie.ReleaseDate;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieOverviewTranslation2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieReleaseDate2;
import io.sfdb.service.SFDBHelper;
import io.sfdb.service.sfdb.SFDbMovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * User: Simo Ala-Kotila
 * Date: 07/01/17
 * Time: 12:53
 */
public class SFDbMovieServiceImpl implements SFDbMovieService {

    private static final Logger LOG = LoggerFactory.getLogger(SFDbMovieServiceImpl.class);

    private final TheMovieDBMovieResource theMovieDBMovieResource;

    private final OMDbMovieResource omdbMovieResource;

    @Autowired
    public SFDbMovieServiceImpl(TheMovieDBMovieResource theMovieDBMovieResource, OMDbMovieResource omdbMovieResource){
        this.theMovieDBMovieResource = theMovieDBMovieResource;
        this.omdbMovieResource = omdbMovieResource;
    }

    @Override
    public void comingSoonInTheatres() {

    }

    @Override
    public void newReleasesInTheatres() {

    }

    @Override
    public void newReleasesPhysicalAndDigital() {

    }

    @Override
    public void comingSoonPhysicalAndDigital() {

    }

    @Override
    public List<Movie> newReleases(int index, String lang) {
        List<Movie> movies = new ArrayList<>();
        Pageable query = new PageRequest(index, SFDBHelper.SQL_MAX_RESULT);
        List<TheMovieDBMovie2> movieByReleaseTypeWithReleaseDate = theMovieDBMovieResource.findMovieByReleaseTypeAndLangWithReleaseDateAndOverview(ReleaseDate.Type.PREMIERE.code, lang, query);
        setMovies(movieByReleaseTypeWithReleaseDate, movies);
        return movies;
    }

    protected void setMovies(List<TheMovieDBMovie2> moviesFromDB, List<Movie> movies){
        for (TheMovieDBMovie2 theMovieDBMovie2 : moviesFromDB) {
            Movie movie = createMovie(theMovieDBMovie2);
            setReleaseDates(theMovieDBMovie2.releaseDate2s, movie);
            setOverview(theMovieDBMovie2.theMovieDBMovieOverviewTranslation2.iterator().next(), movie);
            movies.add(movie);
        }
    }

    protected void setOverview(TheMovieDBMovieOverviewTranslation2 overview, Movie movie) {
        movie.lang = overview.ISO_639_1;
        movie.plot = overview.overview;
    }

    protected void setReleaseDates(Set<TheMovieDBMovieReleaseDate2> releaseDate2s, Movie movie){
        for (TheMovieDBMovieReleaseDate2 releaseDate2 : releaseDate2s) {
            ReleaseDate releaseDate = createReleaseDate(releaseDate2);
            movie.releaseDates.add(releaseDate);
        }
    }

    protected String createPosterURL(String theMovieDBPoster, String imdbId){
        if(Strings.isNullOrEmpty(imdbId)){
            return SFDBHelper.theMovieDBPosterPath(650, theMovieDBPoster);
        }
        OMDbMovie byImdbId = omdbMovieResource.findByImdbId(imdbId);
        if(byImdbId == null){
            return SFDBHelper.theMovieDBPosterPath(650, theMovieDBPoster);
        }
        if(Strings.isNullOrEmpty(byImdbId.getPoster_path())){
            return SFDBHelper.theMovieDBPosterPath(650, theMovieDBPoster);
        }
        return byImdbId.getPoster_path();
    }

    protected Movie createMovie(TheMovieDBMovie2 theMovieDBMovie2){
        Movie movie = new Movie();
        movie.originalTitle = theMovieDBMovie2.original_title;
        movie.title = theMovieDBMovie2.title;
        movie.imdbId = theMovieDBMovie2.imdbId;
        movie.movie_db_id = theMovieDBMovie2.id;

        movie.poster = createPosterURL(theMovieDBMovie2.poster_path, movie.imdbId);
        return movie;
    }

    protected ReleaseDate createReleaseDate(TheMovieDBMovieReleaseDate2 releaseDate2){
        ReleaseDate releaseDate = new ReleaseDate();
        releaseDate.releaseDate = releaseDate2.release_date;
        releaseDate.ISO_639_1 = releaseDate2.ISO_639_1;
        releaseDate.type = ReleaseDate.Type.fromCode(releaseDate2.type);
        return releaseDate;
    }


    @Override
    public void comingSoon() {

    }

    @Override
    public List<Movie> all(int index) {
        return null;
    }
}