package io.sfdb.service.themoviedb;

import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBDBelongsToCollection;

import java.util.Locale;

/**
 * User: Simo Ala-Kotila
 * Date: 23/04/2017
 * Time: 13.20
 */
public interface TheMovieDBCollectionServiceV2 {

    boolean saveCollection(TheMovieDBDBelongsToCollection belongsToCollection);

    boolean saveCollectionTranslation(String name, int collectionId, Locale locale);

    boolean saveCollectionTranslation(int collectionId, Locale locale);
}
