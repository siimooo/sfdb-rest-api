package io.sfdb.service.themoviedb.impl;

import io.rxjavathemoviedb.network.moshi.movie.*;
import io.rxjavathemoviedb.network.movies.TheMovieDBClient;
import io.sfdb.resource.sfdb.model.movie.ReleaseDate;
import io.sfdb.resource.themoviedb.TheMovieDBMovieOverviewTranslationResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieReleaseDateResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieOverviewTranslation2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieReleaseDate2;
import io.sfdb.service.themoviedb.TheMovieDBCollectionServiceV2;
import io.sfdb.service.themoviedb.TheMovieDBMovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * User: Simo Ala-Kotila
 * Date: 20/01/17
 * Time: 10:52
 */
public class TheMovieDBMovieServiceImpl implements TheMovieDBMovieService {

    private static final Logger LOG = LoggerFactory.getLogger(TheMovieDBMovieServiceImpl.class);

    private final TheMovieDBClient theMovieDBClient;

    private final TheMovieDBMovieResource theMovieDBMovieResource;

    private final TheMovieDBCollectionServiceV2 theMovieDBCollectionService;

    private final TheMovieDBMovieOverviewTranslationResource theMovieDBMovieOverviewTranslationResource;

    private final TheMovieDBMovieReleaseDateResource theMovieDBMovieReleaseDateResource;

    @Autowired
    public TheMovieDBMovieServiceImpl(TheMovieDBClient theMovieDBClient, TheMovieDBMovieResource theMovieDBMovieResource, TheMovieDBCollectionServiceV2 theMovieDBCollectionService, TheMovieDBMovieOverviewTranslationResource theMovieDBMovieOverviewTranslationResource, TheMovieDBMovieReleaseDateResource theMovieDBMovieReleaseDateResource){
        this.theMovieDBClient = theMovieDBClient;
        this.theMovieDBMovieResource = theMovieDBMovieResource;
        this.theMovieDBCollectionService = theMovieDBCollectionService;
        this.theMovieDBMovieOverviewTranslationResource = theMovieDBMovieOverviewTranslationResource;
        this.theMovieDBMovieReleaseDateResource = theMovieDBMovieReleaseDateResource;
    }

    @Override
    public TheMovieDBUpcoming upcoming(Integer index) {
        return theMovieDBClient.upcoming(index).toBlocking().first();
    }

    @Override
    public List<TheMovieDBMovieUpcomingResults> upcomings(){
        TheMovieDBUpcoming theMovieDBUpcoming = upcoming(1);
        int totalPages = theMovieDBUpcoming.getTotal_pages();
        List<TheMovieDBMovieUpcomingResults> upcomingResultses = new ArrayList<>();
        for (int i = 1; i <= totalPages; i++) {
            TheMovieDBUpcoming theMovieDBUpcoming1 = upcoming(i);
            for(TheMovieDBMovieUpcomingResults results : theMovieDBUpcoming1.getResults()){
                upcomingResultses.add(results);
            }
        }
        return upcomingResultses;
    }

    @Override
    public boolean isMovieDetailEmpty() {
        Pageable paginable = new PageRequest(0, 1);
        Page page = theMovieDBMovieResource.findAll(paginable);
        List<TheMovieDBMovie2> theMovieDBMovie2s = page.getContent();
        return theMovieDBMovie2s.isEmpty();
    }

    @Override
    public TheMovieDBDetails details(int movieDbId) {
        return details(movieDbId, Locale.ENGLISH);
    }

    @Override
    public TheMovieDBDetails details(int movieDbId, Locale locale) {
        return theMovieDBClient.details(movieDbId, locale).delay(180, TimeUnit.MILLISECONDS).toBlocking().first();
    }


    @Override
    public boolean saveUpcoming(TheMovieDBMovieUpcomingResults upcomingResult) {
        return saveMovieDetails(upcomingResult.getId());
    }

    @Override
    public List<TheMovieDBMovieReleaseDateResults> releaseDates(int movieDbId) {
        TheMovieDBMovieReleaseDate first = theMovieDBClient.releaseDate(movieDbId).delay(180, TimeUnit.MILLISECONDS).toBlocking().first();
        List<TheMovieDBMovieReleaseDateResults> results = first.getResults();
        if(results == null){
            return new ArrayList<>();
        }
        return results;
    }

    @Override
    public int saveUpcomings(List<TheMovieDBMovieUpcomingResults> upcomingResults) {
        int count = 0;
        for(TheMovieDBMovieUpcomingResults results : upcomingResults){
            boolean b = saveUpcoming(results);
            if(b){
                count++;
            }
        }
        return count;
    }



    @Override
    public boolean saveMovieDetails(int movieDBId) {
        TheMovieDBDetails details = details(movieDBId);
        return saveMovieDetails(details);
    }

    @Override
    public boolean saveMovieDetails(TheMovieDBDetails details) {
        int movieDBId = details.getId();
        TheMovieDBMovie2 theMovieDBMovie2 = theMovieDBMovieResource.findByTheMovieDbId(movieDBId);
        TheMovieDBDBelongsToCollection belongs_to_collection = details.getBelongs_to_collection();
        boolean isCollectionSaved = theMovieDBCollectionService.saveCollection(belongs_to_collection);
        if(isCollectionSaved){
            String name = belongs_to_collection.getName();
            int collectionId = belongs_to_collection.getId();
            theMovieDBCollectionService.saveCollectionTranslation(name, collectionId, Locale.ENGLISH);
        }

        boolean isSave = false;
        if(theMovieDBMovie2 == null){
            TheMovieDBMovie2 saveObject = detailsToDB(details, belongs_to_collection);
            theMovieDBMovieResource.save(saveObject);
            isSave = true;
        }

        String overview = details.getOverview();
        saveOverview(movieDBId, overview, Locale.ENGLISH);
        return isSave;
    }

    @Override
    public boolean saveOverview(int movieDBId, String overview, Locale locale) {
        String lang = locale.getLanguage();
        TheMovieDBMovieOverviewTranslation2 theMovieDBMovieOverviewTranslation2 = theMovieDBMovieOverviewTranslationResource.findByMovieDBIdAndLanguage(movieDBId, lang);
        if(theMovieDBMovieOverviewTranslation2 == null){
            TheMovieDBMovieOverviewTranslation2 overviewTranslation2 = new TheMovieDBMovieOverviewTranslation2();
            overviewTranslation2.overview = overview;
            overviewTranslation2.ISO_639_1 = locale.getLanguage();
            overviewTranslation2.the_movie_db_id = movieDBId;
            theMovieDBMovieOverviewTranslationResource.save(overviewTranslation2);
            return true;
        }
        return false;
    }

    @Override
    public boolean saveOverview(int movieDBId, Locale locale) {
        TheMovieDBDetails details = details(movieDBId, locale);
        String overview = details.getOverview();
        return saveOverview(movieDBId, overview, locale);
    }

    @Override
    public int saveOverview(List<TheMovieDBMovieUpcomingResults> upcomingResults, Locale locale) {
        int count = 0;
        for(TheMovieDBMovieUpcomingResults results : upcomingResults){

            boolean b = saveOverview(results.getId(), locale);
            if(b){
                count++;
            }
        }
        return count;
    }

    @Override
    public void saveReleaseDates(int movieDbId) {

        List<TheMovieDBMovieReleaseDateResults> theMovieDBMovieReleaseDateResultses = releaseDates(movieDbId);
        for (TheMovieDBMovieReleaseDateResults theMovieDBMovieReleaseDateResultse : theMovieDBMovieReleaseDateResultses) {
            List<TheMovieDBMovieReleaseDates> release_dates = theMovieDBMovieReleaseDateResultse.getRelease_dates();
            if(release_dates == null){
                LOG.warn("saveReleaseDates have null list for movieDbId={}", movieDbId);
            }
            for (TheMovieDBMovieReleaseDates release_date : release_dates) {
                String iso_639_1 = release_date.getIso_639_1();
                Date release_date1 = release_date.getRelease_date();
                String certification = release_date.getCertification();
                int type = release_date.getType();
                String note = release_date.getNote();

                TheMovieDBMovieReleaseDate2 byMovieDBIdAndLanguageAndType = theMovieDBMovieReleaseDateResource.findByMovieDBIdAndLanguageAndType(movieDbId, iso_639_1, type);
                if(byMovieDBIdAndLanguageAndType == null){
                    TheMovieDBMovieReleaseDate2 theMovieDBMovieReleaseDate2 = new TheMovieDBMovieReleaseDate2();
                    theMovieDBMovieReleaseDate2.the_movie_db_id = movieDbId;
                    theMovieDBMovieReleaseDate2.ISO_639_1 = iso_639_1;
                    theMovieDBMovieReleaseDate2.certification = certification;
                    theMovieDBMovieReleaseDate2.note = note;
                    theMovieDBMovieReleaseDate2.type = type;
                    theMovieDBMovieReleaseDate2.release_date = release_date1;
                    theMovieDBMovieReleaseDateResource.save(theMovieDBMovieReleaseDate2);
                }
            }
        }
    }

    @Override
    public void saveReleaseDates(List<TheMovieDBMovieUpcomingResults> upcomingResults) {
        for (TheMovieDBMovieUpcomingResults upcomingResult : upcomingResults) {
            int id = upcomingResult.getId();
            saveReleaseDates(id);
        }
    }

    protected TheMovieDBMovie2 detailsToDB(TheMovieDBDetails details, TheMovieDBDBelongsToCollection belongs_to_collection){
        TheMovieDBMovie2 theMovieDBMovie2 = new TheMovieDBMovie2();
        theMovieDBMovie2.theMovieDbId = details.getId();
        theMovieDBMovie2.imdbId = details.getImdb_id();
        theMovieDBMovie2.backdropPath = details.getBackdrop_path();
        theMovieDBMovie2.budget = details.getBudget();
        theMovieDBMovie2.original_language = details.getOriginalLanguage();
        theMovieDBMovie2.original_title = details.getOriginal_title();
        theMovieDBMovie2.popularity = details.getPopularity();
        theMovieDBMovie2.poster_path = details.getPosterPath();
        theMovieDBMovie2.revenue = details.getRevenue();
        theMovieDBMovie2.runtime = details.getRuntime();
        theMovieDBMovie2.status = details.getStatus();
        theMovieDBMovie2.tagline = details.getTagline();
        theMovieDBMovie2.title = details.getTitle();
        theMovieDBMovie2.video = details.isVideo();
        theMovieDBMovie2.vote_average = details.getVote_average();
        theMovieDBMovie2.vote_count = details.getVoteCount();
        if(belongs_to_collection == null){
            return theMovieDBMovie2;
        }
        theMovieDBMovie2.belongs_to_collection = belongs_to_collection.getId();
        return theMovieDBMovie2;
    }
}