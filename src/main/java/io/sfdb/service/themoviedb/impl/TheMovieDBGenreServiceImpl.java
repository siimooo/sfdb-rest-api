package io.sfdb.service.themoviedb.impl;

import io.rxjavathemoviedb.network.genre.TheMovieDBGenreClient;
import io.rxjavathemoviedb.network.moshi.TheMovieDBGenre;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreListResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreListTranslationResource;
import io.sfdb.resource.themoviedb.TheMovieDBMovieGenreResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreList2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreListTranslation2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovieGenre2;
import io.sfdb.service.themoviedb.TheMovieDBGenreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Locale;

/**3
 * User: Simo Ala-Kotila
 * Date: 21/01/17
 * Time: 13:01
 */
public class TheMovieDBGenreServiceImpl implements TheMovieDBGenreService {

    private static final Logger LOG = LoggerFactory.getLogger(TheMovieDBGenreServiceImpl.class);

    private final TheMovieDBGenreClient theMovieDBGenreClient;
    private final TheMovieDBMovieGenreListResource theMovieDBMovieGenreListResource;
    private final TheMovieDBMovieGenreResource theMovieDBMovieGenreResource;
    private final TheMovieDBMovieGenreListTranslationResource theMovieDBMovieGenreListTranslationResource;

    @Autowired
    public TheMovieDBGenreServiceImpl(TheMovieDBGenreClient theMovieDBGenreClient, TheMovieDBMovieGenreListResource theMovieDBMovieGenreListResource, TheMovieDBMovieGenreResource theMovieDBMovieGenreResource, TheMovieDBMovieGenreListTranslationResource theMovieDBMovieGenreListTranslationResource){
        this.theMovieDBGenreClient = theMovieDBGenreClient;
        this.theMovieDBMovieGenreListResource = theMovieDBMovieGenreListResource;
        this.theMovieDBMovieGenreResource = theMovieDBMovieGenreResource;
        this.theMovieDBMovieGenreListTranslationResource = theMovieDBMovieGenreListTranslationResource;
    }

    @Override
    public void saveMovieGenres(List<TheMovieDBMovieUpcomingResults> upcomingResults) {
        for(TheMovieDBMovieUpcomingResults results : upcomingResults){
            if(results.getGenreIds() != null){
                saveMovieGenre(results.getGenreIds(), results.getId());
            }
        }
    }

    @Override
    public void saveGenreList(TheMovieDBGenre theMovieDBGenre) {
        saveGenreList(theMovieDBGenre, Locale.ENGLISH);
    }

    @Override
    public void saveGenreList(TheMovieDBGenre theMovieDBGenre, Locale locale) {
        Integer id = theMovieDBGenre.getId();
        TheMovieDBGenreList2 theMovieDBGenre2 = theMovieDBMovieGenreListResource.findTheMovieGenreByGenreId(id);
        if(theMovieDBGenre2 == null){
            TheMovieDBGenreList2 theMovieDBGenreList2 = new TheMovieDBGenreList2();
            theMovieDBGenreList2.genreId = id;
            theMovieDBMovieGenreListResource.save(theMovieDBGenreList2);
        }
        saveGenreListTranslation(theMovieDBGenre, locale);
    }

    @Override
    public void saveGenreListTranslation(TheMovieDBGenre theMovieDBGenre, Locale locale) {
        Integer id = theMovieDBGenre.getId();
        String name = theMovieDBGenre.getName();
        String language = locale.getLanguage();
        TheMovieDBGenreListTranslation2 byGenreIdAndLanguage = theMovieDBMovieGenreListTranslationResource.findByGenreIdAndLanguage(id, language);
        if(byGenreIdAndLanguage == null){
            TheMovieDBGenreListTranslation2 theMovieDBGenreListTranslation2 = new TheMovieDBGenreListTranslation2();
            theMovieDBGenreListTranslation2.genreId = id;
            theMovieDBGenreListTranslation2.ISO_639_1 = language;
            theMovieDBGenreListTranslation2.name = name;
            theMovieDBMovieGenreListTranslationResource.save(theMovieDBGenreListTranslation2);
        }
    }

    @Override
    public void saveGenreListTranslations(List<TheMovieDBGenre> theMovieDBGenres, Locale locale) {
        for(TheMovieDBGenre theMovieDBGenre : theMovieDBGenres){
            saveGenreList(theMovieDBGenre, locale);
        }
    }

    @Override
    public void saveGenreListTranslations(List<TheMovieDBGenre> theMovieDBGenres) {
        saveGenreListTranslations(theMovieDBGenres, Locale.ENGLISH);
    }

    @Override
    public void saveMovieGenre(List<Integer> genreIds, int theMovieDbId) {
        for (Integer genreId : genreIds) {
            if(genreId == null){
                LOG.warn("saveMovieGenre have null genreIds {} theMovieDbId {}", genreIds, theMovieDbId);
            }else{
                TheMovieDBMovieGenre2 theMovieDBMovieGenre2 = new TheMovieDBMovieGenre2();
                theMovieDBMovieGenre2.genre_id = genreId;
                theMovieDBMovieGenre2.theMovieDbId = theMovieDbId;
                theMovieDBMovieGenreResource.save(theMovieDBMovieGenre2);
            }
        }
    }

    @Override
    public List<TheMovieDBGenre> genres() {
        return genres(Locale.ENGLISH);
    }

    @Override
    public List<TheMovieDBGenre> genres(Locale locale) {
        return theMovieDBGenreClient.movieGenres(locale).toBlocking().first().getGenres();
    }

    @Override
    public boolean isGenresEmpty() {
        Pageable paginable = new PageRequest(0, 1);
        Page page = theMovieDBMovieGenreListResource.findAll(paginable);
        List<TheMovieDBGenreList2> genre2s = page.getContent();
        return genre2s.isEmpty();
    }

    @Override
    public void clearDB() {
        theMovieDBMovieGenreResource.deleteAll();
        theMovieDBMovieGenreListTranslationResource.deleteAll();
        theMovieDBMovieGenreListResource.deleteAll();
    }
}