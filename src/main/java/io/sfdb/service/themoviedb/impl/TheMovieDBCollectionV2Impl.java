package io.sfdb.service.themoviedb.impl;

import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBDBelongsToCollection;
import io.sfdb.resource.themoviedb.TheMovieDBCollectionResource;
import io.sfdb.resource.themoviedb.TheMovieDBCollectionTranslationResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBCollection2;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBCollectionTranslation2;
import io.sfdb.service.themoviedb.TheMovieDBCollectionServiceV2;
import org.springframework.stereotype.Service;

import java.util.Locale;

/**
 * User: Simo Ala-Kotila
 * Date: 23/04/2017
 * Time: 13.15
 */

public class TheMovieDBCollectionV2Impl implements TheMovieDBCollectionServiceV2 {

    private final TheMovieDBCollectionTranslationResource theMovieDBCollectionTranslationResource;

    private final TheMovieDBCollectionResource theMovieDBCollectionResource;

    public TheMovieDBCollectionV2Impl(TheMovieDBCollectionTranslationResource theMovieDBCollectionTranslationResource, TheMovieDBCollectionResource theMovieDBCollectionResource){
        this.theMovieDBCollectionTranslationResource = theMovieDBCollectionTranslationResource;
        this.theMovieDBCollectionResource = theMovieDBCollectionResource;
    }

    @Override
    public boolean saveCollection(TheMovieDBDBelongsToCollection belongsToCollection) {
        if(belongsToCollection == null){
            return false;
        }
        Integer collectionId = belongsToCollection.getId();
        TheMovieDBCollection2 byCollectionId = theMovieDBCollectionResource.findByCollectionId(collectionId);
        if(byCollectionId == null){
            TheMovieDBCollection2 theMovieDBCollection2 = new TheMovieDBCollection2();
            theMovieDBCollection2.poster_path = null;
            theMovieDBCollection2.backdrop_path = null;
            theMovieDBCollection2.collectionId = collectionId;
            theMovieDBCollectionResource.save(theMovieDBCollection2);
            return true;
        }
        return false;
    }

    @Override
    public boolean saveCollectionTranslation(String name, int collectionId, Locale locale) {
        TheMovieDBCollectionTranslation2 byCollectionIdIdAndLanguage = theMovieDBCollectionTranslationResource.findByCollectionIdIdAndLanguage(collectionId, locale.getLanguage());
        if(byCollectionIdIdAndLanguage == null){
            TheMovieDBCollectionTranslation2 theMovieDBCollectionTranslation2 = new TheMovieDBCollectionTranslation2();
            theMovieDBCollectionTranslation2.name = name;
            theMovieDBCollectionTranslation2.ISO_639_1 = locale.getLanguage();
            theMovieDBCollectionTranslation2.collectionId = collectionId;
            theMovieDBCollectionTranslationResource.save(theMovieDBCollectionTranslation2);
            return true;
        }
        return false;
    }

    @Override
    public boolean saveCollectionTranslation(int collectionId, Locale locale) {
        String name = "";//query from http://api.themoviedb.org/3/collection/11?api_key=2f1d4601461d8674929cfcdd93e76953&language=fi
        return saveCollectionTranslation(name, collectionId, locale);
    }
}
