package io.sfdb.service.themoviedb;

import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBDetails;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieReleaseDateResults;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBUpcoming;
import io.sfdb.resource.sfdb.model.movie.ReleaseDate;

import java.util.List;
import java.util.Locale;

/**
 * User: Simo Ala-Kotila
 * Date: 20/01/17
 * Time: 09:24
 */
public interface TheMovieDBMovieService {

    int saveUpcomings(List<TheMovieDBMovieUpcomingResults> upcomingResults);

    boolean saveUpcoming(TheMovieDBMovieUpcomingResults upcomingResult);

    List<TheMovieDBMovieReleaseDateResults> releaseDates(int movieDbId);

    List<TheMovieDBMovieUpcomingResults> upcomings();

    TheMovieDBDetails details(int movieDbId);

    TheMovieDBDetails details(int movieDbId, Locale locale);

    boolean isMovieDetailEmpty();

    TheMovieDBUpcoming upcoming(Integer index);

    boolean saveMovieDetails(int movieDBId);

    boolean saveMovieDetails(TheMovieDBDetails details);

    void saveReleaseDates(int movieDbId);

    void saveReleaseDates(List<TheMovieDBMovieUpcomingResults> upcomingResults);

    boolean saveOverview(int movieDBId, String overview, Locale locale);

    boolean saveOverview(int movieDBId, Locale locale);

    int saveOverview(List<TheMovieDBMovieUpcomingResults> upcomingResults, Locale locale);
}