package io.sfdb.service.themoviedb;

import io.rxjavathemoviedb.network.moshi.TheMovieDBGenre;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBGenreListTranslation2;

import java.util.List;
import java.util.Locale;

/**
 * User: Simo Ala-Kotila
 * Date: 20/01/17
 * Time: 16:28
 */
public interface TheMovieDBGenreService {

    void saveMovieGenres(List<TheMovieDBMovieUpcomingResults> upcomingResults);

    void saveGenreList(TheMovieDBGenre theMovieDBGenre);

    void saveGenreList(TheMovieDBGenre theMovieDBGenre, Locale locale);

    void saveGenreListTranslation(TheMovieDBGenre theMovieDBGenre, Locale locale);

    void saveGenreListTranslations(List<TheMovieDBGenre> theMovieDBGenre, Locale locale);

    void saveGenreListTranslations(List<TheMovieDBGenre> theMovieDBGenre);

    void saveMovieGenre(List<Integer> genreIds, int theMovieDbId);

    List<TheMovieDBGenre> genres();

    List<TheMovieDBGenre> genres(Locale locale);

    boolean isGenresEmpty();

    void clearDB();
}
