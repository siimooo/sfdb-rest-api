package io.sfdb.service.omdb.impl;

import io.rxjavaomdb.network.OMDbClient;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;
import io.rxjavaomdb.utils.Parser;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.sfdb.resource.omdb.OMDbMovieResource;
import io.sfdb.resource.omdb.model.OMDbMovie;
import io.sfdb.resource.themoviedb.TheMovieDBMovieResource;
import io.sfdb.resource.themoviedb.model.movie.TheMovieDBMovie2;
import io.sfdb.service.omdb.OMDbService;
import io.sfdb.service.themoviedb.impl.TheMovieDBGenreServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * Implement the OMDb service
 *
 * User: Simo Ala-Kotila
 * Date: 06/01/17
 * Time: 13:00
 */
public class OMDbServiceImpl implements OMDbService{

    private final OMDbClient omDbClient;

    private final OMDbMovieResource omDbMovieResource;

    private final TheMovieDBMovieResource theMovieDBMovieResource;


    private static final Logger LOG = LoggerFactory.getLogger(OMDbServiceImpl.class);

    @Autowired
    public OMDbServiceImpl(OMDbClient omDbClient, OMDbMovieResource omDbMovieResource, TheMovieDBMovieResource theMovieDBMovieResource){
        this.omDbClient = omDbClient;
        this.omDbMovieResource = omDbMovieResource;
        this.theMovieDBMovieResource = theMovieDBMovieResource;
    }

    @Override
    public OMDbTitleOrId fromOMDb(String imdbId) {
        try {
            OMDbTitleOrId first = omDbClient.requestByIMDbId(imdbId, true).toBlocking().first();
            return first;
        }catch (Exception a){
            LOG.error("error on fromOMDb error={}", a.getMessage());
            return null;
        }
    }

    @Override
    public int saveMovies(List<TheMovieDBMovieUpcomingResults> upcomingResults) {
        int count = 0;
        for(TheMovieDBMovieUpcomingResults results : upcomingResults){
            int id = results.getId();

            TheMovieDBMovie2 byTheMovieDbId = theMovieDBMovieResource.findByTheMovieDbId(id);
            if(byTheMovieDbId != null){
                boolean b = saveMovie(id, true);
                if(b){
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public boolean saveMovie(int movieDbId) {
        TheMovieDBMovie2 byTheMovieDbId = theMovieDBMovieResource.findByTheMovieDbId(movieDbId);
        String imdbId = byTheMovieDbId.imdbId;
        return saveMovie(imdbId);
    }

    @Override
    public boolean saveMovie(int movieDbId, boolean isSave) {
        if(!isSave){
            return false;
        }
        return saveMovie(movieDbId);
    }

    @Override
    public boolean saveMovie(String imdbId) {
        OMDbTitleOrId omDbTitleOrId = fromOMDb(imdbId);
        if(omDbTitleOrId == null){
            return false;
        }
        return saveMovie(omDbTitleOrId);
    }

    @Override
    public boolean saveMovie(String imdbId, boolean isSave) {
        if(!isSave){
            return false;
        }
        return saveMovie(imdbId);
    }

    @Override
    public boolean saveMovie(OMDbTitleOrId omDbTitleOrId) {
        String plot = omDbTitleOrId.getPlot();
        String poster = Parser.setPosterRes(omDbTitleOrId.getPoster(), 650);
        String imdbId = omDbTitleOrId.getImdbID();
        String title = omDbTitleOrId.getTitle();
        Date released = omDbTitleOrId.getReleased();
        OMDbMovie omDbMovie = omDbMovieResource.findByImdbId(imdbId);
        if(omDbMovie == null){
            omDbMovieResource.save(new OMDbMovie(imdbId, released, title, poster, plot));
            return true;
        }
        return false;
    }

    @Override
    public boolean saveMovie(OMDbTitleOrId omDbTitleOrId, boolean isSave) {
        if(!isSave){
            return false;
        }
        return saveMovie(omDbTitleOrId);
    }

    @Override
    public void series() {

    }
}
