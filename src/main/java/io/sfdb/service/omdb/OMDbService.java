package io.sfdb.service.omdb;

import io.rxjavaomdb.network.moshi.OMDbTitleOrId;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;

import java.util.List;

/**
 * Service for the OMDb
 *
 * User: Simo Ala-Kotila
 * Date: 06/01/17
 * Time: 12:59
 */
public interface OMDbService {

    OMDbTitleOrId fromOMDb(String imdbId);

    int saveMovies(List<TheMovieDBMovieUpcomingResults> upcomingResults);

    boolean saveMovie(int movieDbId);

    boolean saveMovie(int movieDbId, boolean isSave);

    boolean saveMovie(String imdbId);

    boolean saveMovie(String imdbId, boolean isSave);

    boolean saveMovie(OMDbTitleOrId omDbTitleOrId);

    boolean saveMovie(OMDbTitleOrId omDbTitleOrId, boolean isSave);

    void series();
}
