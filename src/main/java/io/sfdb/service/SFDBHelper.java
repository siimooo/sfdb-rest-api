package io.sfdb.service;

/**
 * User: Simo Ala-Kotila
 * Date: 29/04/2017
 * Time: 15.42
 */
public class SFDBHelper {

    public static final int SQL_MAX_RESULT = 10;

    public static String theMovieDBPosterPath(int size, String path){
        return "http://image.tmdb.org/t/p/w"+size+"/"+path;
    }

}