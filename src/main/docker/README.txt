docker ps -a
docker run <image>
docker start <id|name>
docker stop <id|name>
#remove container
docker rm <id|name>
#remove image
docker rm <id|name>

# Inspect Docker networks
docker network ls
docker network inspect <name>

# Delete all containers
docker rm $(docker ps -a -q)
# Delete all images
docker rmi $(docker images -q)

# deleta all volume
docker volume rm $(docker volume ls -qf dangling=true)

docker exec -it <containerIdOrName> bash
docker exec -it mysql-server bash
docker exec -it sfdb-rest-api bash
https://hub.docker.com/_/redis/
mysql -proot -u root sfdb

docker cp sfdb-rest-api:/tmp/sfdb-rest-api.log /Users/simoala-kotila/Desktop/

INSTALL DOCKER ON RHEL 6.7
--------------------------
Only version 1.7.x available.
REFERENCE
---------
http://goinbigdata.com/docker-run-vs-cmd-vs-entrypoint/
https://www.cyberciti.biz/faq/mysql-command-to-show-list-of-databases-on-server/
https://medium.com/@lhartikk/development-environment-in-spring-boot-with-docker-734ad6c50b34#.drvgag9lr
http://www.liquidweb.com/kb/how-to-install-docker-on-centos-6/
https://prakhar.me/docker-curriculum/
https://github.com/wsargent/docker-cheat-sheet
https://getcarina.com/docs/tutorials/backup-restore-data/
INSTALL
-------
rpm -iUvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
yum update -y
yum -y install docker-io
service docker start
chkconfig docker on

We start with a “standard” image that already includes Java 8 installed (called “Java” and tagged “8”)
We then define that a volume named /tmp should exist
We then add a file from the local filesystem, naming it “app.jar.” The re-naming isn’t necessary, just an option available
We state that we want to open port 8080 on the container
We run a command on the system to “touch” the file. This ensures a file modification release_date on the app.jar file
The ENTRYPOINT command is the “what to run to ‘start'” command — we run Java, setting our Spring Mongo property and a quick additional property to speed up the Tomcat startup time, and then point it at our jar


-- SFDb Movie --
--CREATE TABLE IF NOT EXISTS sfdb_movie (
--    id INT(10) UNSIGNED AUTO_INCREMENT,
--    omdb_id VARCHAR(9),
--    the_movie_db_id INT(10) UNSIGNED,
--    FOREIGN KEY (omdb_id) REFERENCES omdb_movie (imdb_id),
--    FOREIGN KEY (the_movie_db_id) REFERENCES the_movie_db_movie (the_movie_db_id),
--    PRIMARY KEY (id)
--);

---- SFDb Ratings --
--CREATE TABLE IF NOT EXISTS sfdb_movie_rating (
--    id INT(10) UNSIGNED AUTO_INCREMENT,
--    sfdb_id INT(10) UNSIGNED,
--    imdb_rating
--    metascore_rating
--    FOREIGN KEY (sfdb_movie) REFERENCES sfdb_movie (id),
--    PRIMARY KEY (id)
--);

--INSERT INTO source (name, source_id) VALUES ('The Movie DB', 1);
--INSERT INTO source (name, source_id) VALUES ('OMDb', 2);

-- drop tables upcoming
-- show databases
-- show tables
-- INSERT INTO upcoming (title) VALUES ('FUUUC');
