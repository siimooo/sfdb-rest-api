CREATE DATABASE IF NOT EXISTS sfdb CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE sfdb;

-- The Movie DB - GenreList --
CREATE TABLE IF NOT EXISTS the_movie_db_movie_genre_list (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    genre_id INT(5) UNSIGNED UNIQUE,
    PRIMARY KEY (id)
);

-- The Movie DB - GenreListTranslation --
CREATE TABLE IF NOT EXISTS the_movie_db_movie_genre_list_translation (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    genre_id INT(5) UNSIGNED,
    ISO_639_1 VARCHAR(2),
    name VARCHAR(200),
    FOREIGN KEY (genre_id) REFERENCES the_movie_db_movie_genre_list (genre_id),
    PRIMARY KEY (id)
);

-- The Movie DB - Collection --
CREATE TABLE IF NOT EXISTS the_movie_db_collection (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    poster_path VARCHAR(100),
    backdrop_path VARCHAR(100),
    collection_id INT(5) UNSIGNED UNIQUE,
    PRIMARY KEY (id)
);


-- The Movie DB - Movie  --
CREATE TABLE IF NOT EXISTS the_movie_db_movie (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    the_movie_db_id INT(10) UNSIGNED UNIQUE,
    belongs_to_collection INT(5) UNSIGNED,
    imdb_id VARCHAR(50),
    backdrop_path VARCHAR(100),
    budget INT(10),
    original_language VARCHAR(2),
    original_title VARCHAR(3000),
    popularity DOUBLE,
    poster_path VARCHAR(100),
    revenue INT(10),
    runtime INT(10),
    status VARCHAR(30),
    tagline VARCHAR(1000),
    title VARCHAR(3000),
    video BOOLEAN,
    vote_average DOUBLE,
    vote_count INT(10),
    FOREIGN KEY (belongs_to_collection) REFERENCES the_movie_db_collection (collection_id),
    PRIMARY KEY (id)
);

-- The Movie DB - MovieReleaseDate --
CREATE TABLE IF NOT EXISTS the_movie_db_movie_release_date (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    the_movie_db_id INT(10) UNSIGNED,
    ISO_639_1 VARCHAR(2),
    certification VARCHAR(600),
    note VARCHAR(2000),
    release_date DATE,
    type INT(1) UNSIGNED,
    FOREIGN KEY (the_movie_db_id) REFERENCES the_movie_db_movie (the_movie_db_id),
    PRIMARY KEY (id)
);

-- The Movie DB - CollectionTranslation --
CREATE TABLE IF NOT EXISTS the_movie_db_collection_translation (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    name VARCHAR(500),
    ISO_639_1 VARCHAR(2),
    collection_id INT(5) UNSIGNED,
    FOREIGN KEY (collection_id) REFERENCES the_movie_db_collection (collection_id),
    PRIMARY KEY (id)
);

-- The Movie DB - MovieOverviewTranslation --
CREATE TABLE IF NOT EXISTS the_movie_db_movie_overview_translation (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    the_movie_db_id INT(10) UNSIGNED,
    overview TEXT(30000),
    ISO_639_1 VARCHAR(2),
    FOREIGN KEY (the_movie_db_id) REFERENCES the_movie_db_movie (the_movie_db_id),
    PRIMARY KEY (id)
);

-- The Movie DB - MovieGenre  --
CREATE TABLE IF NOT EXISTS the_movie_db_movie_genre (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    genre_id INT(5) UNSIGNED,
    the_movie_db_id INT(10) UNSIGNED,
    FOREIGN KEY (genre_id) REFERENCES the_movie_db_movie_genre_list (genre_id),
    FOREIGN KEY (the_movie_db_id) REFERENCES the_movie_db_movie (the_movie_db_id),
    PRIMARY KEY (id)
);

-- The Movie DB - TV --
CREATE TABLE IF NOT EXISTS the_movie_db_tv (
    id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(200) NOT NULL
);

-- OMDb Movie --
CREATE TABLE IF NOT EXISTS omdb_movie (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    imdb_id VARCHAR(50) UNIQUE,
    released DATE,
    title VARCHAR(3000),
    poster_path VARCHAR(500),
    plot TEXT(30000),
    PRIMARY KEY(id)
);

-- OMDb TV --
CREATE TABLE IF NOT EXISTS omdb_tv (
    id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(200) NOT NULL
);

