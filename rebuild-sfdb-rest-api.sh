#!/usr/bin/env bash
docker stop sfdb-rest-api

#docker stop mysql-server

docker rm sfdb-rest-api

docker rmi siimoo/sfdb-rest-api

#docker start mysql-server

gradle clean

gradle build -x test

docker build -t siimoo/sfdb-rest-api .

docker run -d -p 8080:8888 --net mysql --name sfdb-rest-api siimoo/sfdb-rest-api