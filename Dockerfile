FROM alpine:3.5
VOLUME /tmp
ADD build/libs/sfdb-rest-api-1.0.jar app.jar
EXPOSE 8888
RUN apk add --update bash && rm -rf /var/cache/apk/*
RUN apk --update add openjdk8-jre

# Set environment
#ENV JAVA_HOME /opt/jdk
#ENV PATH ${PATH}:${JAVA_HOME}/bin

RUN sh -c 'touch /app.jar'
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]