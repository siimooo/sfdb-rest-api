#!/usr/bin/env bash

docker stop sfdb-rest-api

docker stop mysql-server

docker rm mysql-server

docker rm mysql-data

docker rm sfdb-rest-api

docker rmi siimoo/sfdb-rest-api